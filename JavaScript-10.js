//Тестирование
//TDD — техника разработки программного обеспечения, которая основывается на повторении очень коротких циклов разработки:
//сначала пишется тест, покрывающий желаемое изменение, затем пишется код, который позволит пройти тест,
// и под конец проводится рефакторингнового кода к соответствующим стандартам.
//Test Driven Development — "Разработка через тестирование"

function expect(value) {
  return {
    toBe: exp => {
      if (value === exp) {
        console.log('Success')
      } else {
        console.log('Error')
      }
    }
  }
}

let sum = (a, b) => a + b;
expect(sum(1, 2)).toBe(3)

//BDD - фрейфорк подход к разработке ПО. Это просто ответвление подхода TDD, 
//Давайте начнём с техники под названием Behavior Driven Development или, коротко, BDD.
//BDD – это три в одном: и тесты, и документация, и примеры использования.
//Best Driven Development — "Разработка через поведение"
//В BDD к тестам на любом этапе может подключиться любой член команды, 
//например, аналитик, бизнес пользователь, разработчик и тестировщик, так как тесты понятны всем участникам процесса.
//BDD еще полезен тем, что не нужно тратить много времени на написание разного рода документации. 
//При классической схеме разработки нужны, как минимум, спецификации и тестовые сценарии, которые обычно пишут разные люди.
//В BDD спецификация является тестовым сценарием, одновременно являясь и автотестом.
//Начало работы с BDD фреймворком — сложно и долго
//В чем преимущество BDD?

//Тесты читаемые для не программистов.
//Их легко изменять. Они часто пишутся почти на чистом английском.
//Их может писать product owner или другие заинтересованные лица.
//Результаты выполнения тестов более "человечные".
//Тесты не зависят от целевого языка программирования. Миграция на другой язык сильно упрощается.

//Сначала задается Feature(История ) Здесь мы задаем на понятном нам языке, что мы хотим увидеть от нашей функциональности. 
//Feature: Показать вошедшее в систему имя пользователя
// Для того чтобы войти в систему как пользователь под названием “Username"
// Я хочу, чтобы в заголовке страницы отображалась подпись

//потом пишеться сценарий
// Здесь мы задаем на понятном нам языке, что мы хотим увидеть от нашей функциональности. 
// Scenario: "Здравствуйте, Username!": Показать вошедшее в систему имя пользователя
// Учитывая, что я вошел в систему как пользователь под названием “User Name"

//если сценариев много то нумеруются
// Scenario: Показать вошедшее в систему имя пользователя
// Given Учитывая, что я вошел в систему как пользователь под названием “Имя пользователя"
// When Когда я посещаю домашнюю страницу
// Then в заголовке страницы появляется подпись "Здравствуйте,Username!“

//отличие tdd от bdd
// TDD хорошо подходит для юнит-тестирования, т.е. для проверки работы отдельных модулей самих по себе. BDD — для интеграционного (т.е. для проверки, как отдельные модули работают друг с другом) и e2e (т.е. для проверки всей системы целиком) тестирования.
//TDD: тесты сразу реализуются в коде, для BDD чаще всего описываются шаги на языке, понятном всем, а не только разработчикам.
// TDD: юнит-тесты пишут сами разработчики. BDD требует объедения усилий разных членов команды. Обычно тест-кейсы (шаги) описываются ручным тестировщиком или аналитиком и воплощаются в код тестировщиком-автоматизатором. В нашей команде мы (фронтенедеры) описываем шаги вместе с тестировщиками, а код тестов пишет фронтенд-команда.
// TDD проверяет работу функций, BDD — пользовательские сценарии.

//karma для запуска  jasmine  с помощью чего тестишь
npm install -g karma
npm karma --version
npm install karma-jasmine
npm install karma-chrome-launcher //подключение к браузеру chrome

karma init
//в скобках указано значение по умолчанию
basePath('') -
files([ 'test.js', 'client/*.js',]) - список файлов/шаблонов для загрузки которые будут тестироваться
exclude(['client/main.js']) - список исключений для предыдущего пункта которые не будут тестироваться
reporters(['progress', 'junit']) - вариант вывода прогресса
port(8080) - порт веб-сервера(testacular)
runnerPort(9100) - порт клиента
colors(true) - включение/выключение цветов при выводе
logLevel(LOG_INFO) - LOG_DISABLE|LOG_ERROR|LOG_WARN|LOG_INFO|LOG_DEBUG
autoWatch(false) - выполнение тестов при изменении файлов
browsers([process.env.TRAVIS ? 'Opera' : 'Chrome']) - Chrome, ChromeCanary, Firefox, Opera, Safari, PhantomJS
captureTimeout(20000) - задание таймаута в миллисекундах
singleRun(false) - для одноразового запуска

karma start //и потом пишем тесты в файле


//Jasmine это bdd тест
//Преимущество
//не имеет зависит от сторонних библиотек, можно использовать сразу из пакета
//можно использовать в браузере так и сервере (Node.js)
//Основными ключевыми словами при работе с Jasmine являются:
// describe — описывает набор тестов и можно внутри использовать describe, 
// it — описывает отдельный тест внутри любого набора тестов
// expect — определяет ожидания, которые проверяются в тесте(соответсвует ли вашему ожиданию)
// Ключевые слова describe и it являются обычными вызовами функций, которым передаются два параметра. 
// Первый — название теста, второй — callback функция содержащая код. 
// использование xdescribe и xit - отключит тест, тоесть будет показывать, но он будет в режие ожидания

// stubs и mock путают: разница в том, что стаб ничего не проверяет, а лишь имитирует заданное состояние. А мок — это объект, у которого есть ожидания. Например, что данный метод класса должен быть вызван определенное число раз. Иными словами, ваш тест никогда не сломается из-за «стаба», а вот из-за мока может.

// stubs
describe("A spy", function() {  
let foo, bar = null;

  beforeEach(function() {
    foo = {
      setBar: function(value) {
        bar = value;
      }
    };

    spyOn(foo, 'setBar').and.callThrough();
  });

  it("can call through and then stub in the same spec", function() {
    foo.setBar(123);
    expect(bar).toEqual(123);

    foo.setBar.and.stub(); 
    bar = null;

    foo.setBar(123); //после stub не измениться
    console.log(bar)
    expect(bar).toBe(null);
  });
});

// mock
async function fetchData(search) {
  try {
    const result = await fetch(
      `url?search=${search}`
    );
    const data = await result.json();
    return data;
  } catch (e) {
    return null;
  }
}

export { fetchData };



describe("Тест Fetch", function () {
  it("удволетворяет условию", async function () {

     var someObject = jasmine.createSpyObj('someObject', ['method1']);
            let obj = someObject.method1.and.callFake(() =>{
             return Promise.resolve( { name:'Artem'  })
            });
    expect(obj).toEqual({name:"Artem"})
  });
  })
describe("Тест Fetch", function () {
  it("удволетворяет условию", async function () {

     var someObject = jasmine.createSpyObj('someObject', ['method1']);
            let obj = someObject.method1.and.callFake(() =>{
             return Promise.reject( { name:'Artem'  })
            });
    expect(obj).toEqual({name:"Artem"})
  });
  })


describe("Набор тестов", function () {
  it("на соответствие сравнивает как ===", function () {
    expect(1 + 2).toBe(3);
  });
  expect(a).toEqual(b) //- тоже сравнивает как ==
  expect(a).not.toEqual(b) //на несовопадение
  expect(a).not.toBe(b) //- отрицание a не является b
  expect(5 > 0).toBeTruthy() //значение должно быть верно true
  expect(5 < 0).toBeFalsy() //значение должно быть не верно false
  expect(1 + 2).toBeLessThan(5) //значение должно быть меньше 5
  expect(1 + 2).toBeGreaterThan(0) //значение должно быть больше 0
  expect(1.2345).toBeCloseTo(1.2, 1) //значение должно быть близко к числу второй параметр 
  //кол-во десятичных разрядов по которуму сравниваем
  expect("some string").toMatch(/string/) //значение должно соответствовать регулярному вырожению
  expect([1, 2, 3]).toContain(2) //значение должно содержать 
  expect("some string").toContain("some") //значение должно содержать
  expect(a).toBeNull() //значение должно быть null
  expect(window.notExists).toBeUndefined() //значение должно быть не определено 
  expect(window.document).toBeDefined() //- значение должно быть определено
  expect(0 / 0).toBeNaN() //- значение для проверки на NaN
  expect(функция).toThrow()//- проверка на вызов исключения
  expect(addAny(9, 9)).toEqual(jasmine.any(Number)) //в функцию передаем числа и потом мы используем any когда
  //мы не уверены выводе, но знаем что это число, если укажем String и тд то будет ошибка

  //для тестирование класса
  class Calculate {
    constructor(a, b) {
      this.a = a;
      this.b = b;
    }
    plus() {
      return this.a + this.b;
    }
  }
  describe("Набор тестов", function () {
    it("проверка класса", function () {
      const calc = new Calculate(2, 3)
      expect(calc.plus()).toBe(5);
    });
  });

  // beforeEach - перед каждым тестом (it) будет выполняться то что мы определили в beforeEach, тоесть если 
  // описали массив [1,2,3], то перед каждым тестом этот массив использоваться, и не важно изменим 
  //ли мы в тесте it, если бы мы объявили глобально в тесте descirbe массив и добавили еще два значения в массив [1,2,3,4,5] в тесте it проверяли на нахождения этих элементов, а в другом тесте it мы ожидали, тот который начальный [1,2,3] и будет,то тогда будет ошибка,поэтому определенный массив в  beforeEche и будет каждый раз запускать перед каждым тестом
  // afterEach - выполняется после каждого теста (it) будет выполняться то что мы определилив afterEach
  // они используются чтобы не потеряться, тоесть выполняя один тест мы можем изменить свойство так что 
  // в следующем тесте мы не сможем спрогназировать что там будет.

  // которые будут выполнены до/после запуска тестов
  //before(function() { alert("Начало тестов"); });
  //after(function() { alert("Конец тестов"); });

// макирование метод который перкрывает базовое поведение

  describe("Набор тестов", function () {
    const calc = new Calculate(2, 3)
    beforeEach(function () {
      calc = new Calculate(2, 3)
      console.log(calc)
    });
    afterEach(function () {
      calc = new Calculate(2, 3)
      console.log(calc)
    });
    it("проверка класса", function () {
      calc.a = 10
      expect(calc.plus()).toBe(5);
    });
    it("проверка класса", function () {
      calc.a = 15
      expect(calc.plus()).toBe(5);
    });
  });


  //можно писать свои матчеры
  beforeEach(function () {
    this.addMatchers({
      toBeBetween: function (rangeFloor, rangeCeiling) {
        if (rangeFloor > rangeCeiling) {
          var temp = rangeFloor;
          rangeFloor = rangeCeiling;
          rangeCeiling = temp;
        }
        return this.actual > rangeFloor && this.actual < rangeCeiling;
      }
    });
  });

  it("is between 5 and 30", function () {
    expect(10).toBeBetween(5, 30);
  });

  it("is between 30 and 500", function () {
    expect(100).toBeBetween(500, 30);
  });

  //асинхронное тестирование 
  describe("Asynchronous specs", function () {
    let value, flag;
    it("async", function () {

      runs(function () {
        flag = false;
        value = 0;

        setTimeout(function () {
          flag = true;
        }, 500);
      });

      waitsFor(function () {
        value++;
        if (flag) {
          console.log("A");
        }
        return flag;
      }, "Значение должно быть увеличично", 750);

      it('should run less than a second', function (done) {
        setTimeout(function () {
          done();
        }, 900);
      }, 1000);

      describe("Async", function () {
        var value;
        beforeEach(function (done) {
          setTimeout(function () {
            value = 0;
            done();
          }, 1);
        });
        it("должен поддерживать асинхронное выполнение подготовки к тестированию и ожиданий", function (done) {
          value++;
          expect(value).toBeGreaterThan(0);
          done();
        });

        //асинхронные запросы

        describe("GET", function () {
          it("get data", async function (done) {
            const response = await fetch('https://api.thecatapi.com/');
            expect(response.status).toBe(200);
            const data = await response.json();
            expect(data).not.toBeUndefined();
            expect({}).toEqual(jasmine.any(Object));
            expect(callback).toHaveBeenCalledWith(jasmine.objectContaining({
              message: "Cat"
            }));
            done();
          })
        });

        //отслеживания вызова функции и параметров вызова осуществляется с помощью spyOn
        // принимает в себя два аргумента это объект за которым следим и метод в 'кавычках'
        describe("Набор тестов", function () {
          let person = null
          beforeEach(function () {
            person = new Calculate("Artem", 25)

          });
          it("осуществлен вызов функции", function () {
            spyOn(person, 'getName')
            person.getName()
            expect(person.getName).toHaveBeenCalled() // проверяет был ли вызван метод
          });

          it("проверка количества вызововов методов закоторым следит шпион", function () {
            spyOn(person, 'addYear')
            person.addYear()
            person.addYear()
            expect(person.addYear.calls.length).toEqual(2)
          });

          it("проверка аргументов", function () {
            spyOn(person, 'setName')
            person.setName("Ira")
            expect(person.setName).toHaveBeenCalledWith("Ira") //# может быть несколько аргументов
          });

          it("есть доступ к последнему вызову", function () {
            spyOn(person, 'setName')
            person.setName("Ira")
            expect(person.setName.mostRecentCall.args[0]).toEqual("Ira")
          });
          it("есть доступ ко всем вызовам", function () {
            spyOn(person, 'setName')
            person.setName("Ira")
            expect(person.setName.calls[0].args[0]).toEqual("Ira")

          });
          it("вызывает оригинальную функцию", function () {
            spyOn(person, 'getName').andCallThrough()
            expect(person.getName()).toEqual("Jim")
            expect(person.getName).toHaveBeenCalled()
          });
          it("возвращает указанное значение", function () {
            spyOn(person, 'getName').andReturn("Dan") //заменяет методу значение
            expect(person.getName()).toEqual("Dan")
            expect(person.getName).toHaveBeenCalled()
          })
          it("вызывает указанную функцию", function () {
            spyOn(person, 'getAge').andCallFake(() => { return 5 * 11 })
            expect(person.getAge()).toEqual(55)
            expect(person.getAge).toHaveBeenCalled()
          })
          it("вызывает указанную функцию", function () {
            var someObject = jasmine.createSpyObj('someObject', ['method1', 'method2']);
            let obj = someObject.method1.and.callFake(function () {
              throw 'an-exception';
            });
            expect(obj).toThrow('an-exception');
          })

          //фейковые функции используются для тестирование асинхронных запросов

          // Для создания функции без реализации можно воспользоваться createSpy, 
          // при этом доступны все возможности для тестирования обычного spy. 
          // Единственный параметр, который принимает createSpy — это имя функции для идентификации.
          it("создает фальшивую функцию", function () {
            concat = jasmine.createSpy('CONCAT')
            concat("one", "two")
            expect(concat.identity).toEqual('CONCAT')// # есть имя для идентификации
            expect(concat).toHaveBeenCalledWith("one", "two")
            expect(concat.calls.length).toEqual(1)
          })

          // Синхронное тестирование вызовов setTimeout/setInterval осуществляется с помощью jasmine.Clock.useMock.
          //  Для перемещения времени вперед используется вызов jasmine.Clock.tick, которому передается время в миллисекундах:

          describe("Время", function () {
            callback = null

            beforeEach(function () {
              callback = jasmine.createSpy('TIMER')
              jasmine.Clock.useMock()
            })
            it("вызывает timeout синхронно", function () {
              setTimeout(function () { callback() }, 100)// # задержка вызова в 100ms
              expect(callback).not.toHaveBeenCalled()
              jasmine.Clock.tick(101) //# передвинуть время на 101ms
              expect(callback).toHaveBeenCalled()
            })
          })

//end-2-end 
//Сквозное тестирование — это тестирование кода от н


//для чего нужны автотесты

//большое количество ручных тестов и не хватает времени на регулярное проведение полного регресса;
//большой процент пропуска ошибок по вине человеческого фактора;
//большой промежуток времени между внесением ошибки, ее обнаружением и исправлением;
//подготовка к тестированию (настройка конфигурации, генерация тестовых данных) занимает много времени;
//большие команды, в которых нужна уверенность, что новый код не сломает код других разработчиков;
//поддержка старых версий ПО, в которых нужно тестировать новые патчи и сервис-паки.

//виды автотестов

//Автотесты принято делить на три типа: модульные (юнит), сервисные и интеграционные  (UI, браузерные, end?to?end, E2E) тесты.

//Модульные тесты проверяют работу отдельно взятых модулей — функций, классов, моделей, контроллеров — кирпичиков, из которых построено приложение. 
//Модульные тесты выполняют проверки за миллисекунды, потому что тестируют модуль в отрыве от остального приложения.

//Интеграционные тесты проверяют приложение целиком, когда все модули в сборе. Их задача — убедиться, что модули правильно соединены друг с другом.
// Кроме того, ими проверяют те части приложения, которые не имеют права сломаться: биллинг, регистрацию, оформление заказа

//Сервисные тесты — среднее между интеграционными и модульными. Их задача — проверить функционал, который нельзя уверенно протестировать модульными тестами, 
//а интеграционными тестировать «слишком жирно».
