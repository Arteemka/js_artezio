﻿//Что такое DOM? Из чего состоит DOM, Узлы и их виды.

//window - это браузерный глобальный объект, который содержит Dom,Bom, и для js

// (Объектная Модель Документа) DOM - это представление документа в виде дерева тегов. 
//Это дерево образуется за счет вложенной структуры тегов плюс текстовые фрагменты страницы, каждый из которых образует отдельный узел.

//В окне просмотра браузера вы видите дерево рендеринга, которое,является комбинацией DOM и CSSOM.
//Чем отличается DOM от дерева рендеринга, тeм что в конечном итоге будет отображено на экране.
//Согласно DOM-модели, документ является иерархией, деревом. Каждый HTML-тег образует узел дерева с типом «элемент».
//Вложенные в него теги становятся дочерними узлами. 
//Для представления текста создаются узлы с типом «текст».
//Текстовый узел text node, узлы-элементы (element node), узел документа, узел комментария,<!--->
//document - это обьект, корень самого документа
  //<html>          //кореневой узел
   //<head>          //дочерний узел
    //<title>Привет</title> //внутри тэга title текстовый узел "Привет"
   //</head>
     //<body>         //дочерний узел
      // Правда . //  текстовый узел "Правда"
     //</body>
  //</html>
//С помощью document ты можешь достучаться к DOM элементам.

//parentNode - возвращает родительский элемент
//parentElement - возвращает родительский элемент, который относятся к элементам

let html = document.documentElement;
console.log(html.parentNode); // выведет document
console.log(html.parentElement); // выведет null

//Поиск элементов
   getElementById(value) // выбирает элемент, у которого атрибут id равен value, результат сохраняется 
   getElementsByTagName(value) // выбирает все элементы, у которых тег равен value, работают с живыми коллекциями HTMLCollection
   getElementsByClassName(value) // выбирает все элементы, которые имеют класс value работают с живыми коллекциями HTMLCollection
   getElementsByName(name) // находит все элементы с заданным атрибутом name. работают с живыми коллекциями HTMLCollection
   querySelector(value) // выбирает первый элемент, который соответствует css - селектору value с неживыми коллекциями
   querySelectorAll(value) // выбирает все элементы, которые соответствуют css - селектору value с неживыми коллекциями NodeList

// htmlcolection и nodelist коллекцию это как псевдомассив имеет только свойства length, для того  чтобы преобразовать нужно Array.from(arr) использовать

//пример
let articles1 = document.getElementsByTagName('article');
let articles2 = document.querySelectorAll('article');

articles1; // [article]
articles2; // [article]

// тег <article> удалён

articles1; // []
articles2; // [article]


const elem = document.getElementById("block")
elem.childNodes // возвращает коллекцию всех дочерних узлов узла родителя,которые представлены ниже
elem.nodeName // возвращает имя узла
elem.nodeType // возвращает тип узла в виде числа
    //1 ELEMENT_NODE	Узел элемента
    //3	TEXT_NODE	Текстовый узел (#text)
    //7	PROCESSING_INSTRUCTION_NODE	Узел инструкции обработки
    //8	COMMENT_NODE	Узел комментария (#comment)
    //9	DOCUMENT_NODE	Узел документа (#document)
    //10	DOCUMENT_TYPE_NODE	Узел типа документа
    //11	DOCUMENT_FRAGMENT_NODE	Узел фрагмента документа

elem.firstChild // предназначен для получения первого узла если же нет потомков то возвращает null
elem.lastChild // предназначен для получения последнего узла если же нет потомков то возвращает null
elem.previousSibling // пердназначен для получения предыдущего узла в childNodes его родительского узла
elem.nextSibling // пердназначен для получения следующего узла в childNodes его родительского узла
elem.children

//методы dom
elem.getAttribute('class') // возвращает значение атрибута attr
elem.setAttribute('class', 'class') // устанавливает для атрибута attr значение value. Если атрибута нет, то он добавляется
elem.removeAttribute('class') // удаляет атрибут attr и его значение

//добавление, удаление классов
elem.classList.add('foo') //добавление класса
elem.classList.remove('bar') //удаление класса
elem.classList.toggle('baz') // удаление и добавление класса 

// изменения в dom
// Добавление element1 как последнего дочернего элемента element2
   element1.appendChild(element2)
// Вставка element2 как дочернего элемента element1 перед element3
   element1.insertBefore(element2, element3);
//для того чтобы удалить элемент можно сделать это через родительский элемент
   myParentElement.removeChild(myElement)
//или
   myElement.parentNode.removeChild(myElement)

elem.append() // добавляет узлы или строки в конец node,
elem.prepend() // вставляет узлы или строки в начало node,
elem.before() // вставляет узлы или строки до node,
elem.after() // вставляет узлы или строки после node
    /* <ol id="ol">
      <li>0</li>
      <li>1</li>
      <li>2</li>
    </ol> */
ol.before('before'); // вставить строку "before" перед <ol>
ol.after('after'); // вставить строку "after" после <ol>

/* <p>before</p> - before
  <ol id="ol">
    <p>prepend</p> - prepend
  <li>0</li>
  <li>1</li>
  <li>2</li>
  <p>append</p> - append
</ol>
<p>after</p> - after */

elem.insertAdjacentHTML('beforebegin', '<p>Привет</p>');
  "beforebegin" // вставить html непосредственно перед elem,
  "afterbegin" // вставить html в начало elem,
  "beforeend" // вставить html в конец elem,
  "afterend" // вставить html непосредственно после elem.

/* <p>Привет</p> - beforebegin
  <ol id="ol">
    <p>Привет</p> - afterbegin
  <li>0</li>
  <li>1</li>
  <li>2</li>
  <p>Привет</p> - beforeend
</ol>
<p>Привет</p>  - afterend */


//BOM
//Объектная модель браузера (Browser Object Model, BOM) – это дополнительные объекты, 
// чтобы работать со всем, кроме документа.

//Функции alert/confirm/prompt тоже являются частью BOM: они не относятся непосредственно к странице, 
//но представляют собой методы объекта окна браузера для коммуникации с пользователем.

//методы
//Объект navigator даёт информацию о самом браузере и операционной системе. 
//Среди множества его свойств самыми известными являются: 
//navigator.userAgent – информация о текущем браузере,
   window.navigator.userAgent
//navigator.platform – информация о платформе (может помочь в понимании того, в какой ОС открыт браузер – Windows/Linux/Mac и так далее).
   window.navigator.platform
//Объект location позволяет получить текущий URL и перенаправить браузер по новому адресу.
   window.location.href //возвращает ссылку (URL) текущей страницы
   window.location.hostname //возвращает доменное имя веб-хоста
   window.location.pathname //возвращает путь и имя файла текущей страницы
   window.location.protocol //возвращает использованный веб-протокол (http: или https:)
   window.location.assign //загружает новый документ
//Объект History предоставляет журнал навигации за всё время работы с конкретным окном.
//Объект Screen сведения о графических параметрах клиентской системы вне окна браузера, такие как ширина и высота в пикселях.
   window.screen.width // ширина экрана в px
//setTimeout
//setInterval