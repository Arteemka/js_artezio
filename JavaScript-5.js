//отличие статических методов от нестатических методов  это метод, который принадлежит классу, но он не принадлежит кземпляру этого класса, и этот метод можно вызывать без экземпляра или объекта этого класса.
//Статический метод принадлежит классу, а нестатический метод принадлежит объекту класса.

// String

// Строку можно задать с помощью кавыек или предназначенный объект String
   const name = 'Artemiy'; // одинарные кавычки
   const secondName = "Rak"; //  двойные кавычки
   const patronymic = new String('Vitalyevich');
   String('Artemiy');
   // обратные кавычки могут принимать произвольные выражения с помощью конструкции ${},
   // кроме этого они могут занимать больше одно строки
   console.log(`Hi ${secondName} ${name} ${patronymic}`); // Hi Rak Artemiy Vitalyevich
   console.log(`FIO:
 
		Rak

		Artemiy
		Vitalyevich`);
//спецсимволы 
 // \n - перевод строки
 // \t - табуляция
 // \',\" - кавычки

//свойства  и методы строк

 // length - определяет длину строки начиная с 1
   console.log('Rak'.length) // 3
 

 // Для доступа к символу можно использовать [] или charAt(), отсчет позиции начинается с 0 
   
   let secondName = 'Rak';

   console.log(secondName[2]) // k
   console.log(secondName.charAt(1)) // a
 // отличие между ними в том если символ с такой позицией отсутствует,то [] вернёт undefined, 
  //  а charAt вернет пустую строку

   console.log(secondName[5]) // undefined
   console.log(secondName.charAt(1)) // ''
 
 // также строку можно перебрать по символьно с помощью for of
 for (let char of "Artemiy") {
  console.log(char); // A,r,t,e,m,i,y 
 }

 // символ строки нельзя заменить, только если создать новую строку и  в туже самую переменную записать
    let str = 'Hi';

    str = 'h' + str[1]; // заменяем строку
    console.log( str ); //hi

 // toLowerCase() и toUpperCase() - меняет регистр в верхний и нижний регистр
    console.log( 'Artemiy'.toUpperCase() ); // ARTEMIY
    console.log( 'Artemiy'.toLowerCase() ); // artemiy

   // Если мы захотим перевести в верхний регистр какой-то конкретный символ:
      console.log( 'Artemiy'[3].toUpperCase() ); // E
 
 // поиск подстроки

   // str.indexOf - возвращает индекс первого вхождения, если не нашол возвращает -1,
     // можно еще указать позицию с какой позиции начинать, если будет в первом аргументе пустая строка
     //и позиция больше чем длина строки,то вернет длину строки, а если позиция меньше или равно,вернет позицию
     'Синий кит'.indexOf('кит', 5); // вернёт  6
     'Синий кит'.indexOf('', 8);  // 8
     'Синий кит'.indexOf('', 10); //9 т.к длина строки 9
     
    // lastIndexOf(substr, position) который ищет с конца строки к её началу
     console.log('Artemiy'.lastIndexOf('t'));     // вернёт 3
     console.log('Artemiy'.lastIndexOf('m', 4));  // вернёт 4

// с условием if есть проблема если ищете символ, а он стоит на 0 позиции то не нечего не сработает т.к преобразует 
// 0 в false
   let str = "Artemiy";

   if (str.indexOf("A")) { console.log("Совпадение есть"); // не работает}
  // чтобы решить проблему нужно добавить != -1 или же использовать ~ (~-1),равняется (-(-1+1))
   let str = "Artemiy";

   if (str.indexOf("A") != -1) { console.log("Совпадение есть"); // теперь работает }
   if (~str.indexOf("A")) { console.log("Совпадение есть");} // теперь работает }

 // includes возвращает true или false, также позволяет начинать с определенной позиции
   console.log( "Hello my name".includes("my") ); // true
   console.log( "Hello my name".includes("Hi") ); // false
   console.log( "Hello my name".includes("m",3) ); // true

 // startsWith и endsWith начинается ли и заканчивается ли строка определённой строкой, позицию тоже можно указать
   console.log( "Hello".startsWith("Hel") ); // true, "Hel" — начало "Hello"
   console.log( "Hello".endsWith("lo") ); // true , "lo" — конец "Hello"
   console.log( "Hello my name is Artemiy, and your name".startsWith("Artemiy",18) ); // true, 
   console.log( "Hello my name is Artemiy, and your name".endsWith("name",13) ); // true , 

// получение под строки substring, substr и slice

  // str.slice(start [, end])
  // Возвращает часть строки от start до (не включая) end.
     console.log( 'hello man'.slice(0, 3) ); // he
  // Если аргумент end отсутствует, slice возвращает символы до конца строки
     console.log( 'hello man'.slice(3) ); // lo man
  // start/end можно задавать отрицательные значения, будет начинаться с конца строки
     console.log( 'hello man'.slice(-5,-1) ); //o ma 
  // если ввести start больше end то вернет пустую строку
     console.log( 'hello man'.slice(3,0) ); ''
 
  // str.substring(start [, end])
  // Возвращает часть строки между start и end.
     console.log( 'hello man'.substring(0, 3) ); // hel
  // Отрицательные значения итерпритируются к 0, но если только start указон то считает с конца
     console.log( 'hello man'.substring(-3) ); // hello man
     console.log( 'hello man'.substring(-3,-1) ); // ''
  // Но можно указать start больше чем end то вернет, то он сработает так как сработал при
  // start меньше end 
     console.log( 'hello man'.substring(0, 3) ); // hel
     console.log( 'hello man'.substring(3, 0) ); // hel
  // если аргумента end нету, то возвращает до конца строки
     console.log( 'hello man'.substring(3) ); // lo man
  // если указать одинаково start и end вернет пустую строку
     console.log( 'hello man'.substring(3,3) );  ''

  // str.substr(start [, length])
  // Возвращает часть строки от start длины length.
     console.log( 'hello man'.substring(2,3) ); // llo
  // если length будет опущем возьмет до конца строки
     console.log( 'hello man'.substring(2) ); // llo man
  // Если параметр start является положительным и он больше, либо равен длине строки, вернёт пустую строку.
     console.log( 'hello man'.substring(9) ); // ''
  // если start отрицательное значение то начнет с 0 идекса
     console.log( 'hello man'.substring(-2,2) ); //he

'string'.slice() //string потму что с 0 начинается
'string'.slice(0) //string потму что с 0 начинается
'string'.slice(1,0) // '' так как start больше end
'string'.slice(1,1) // '' так как start и end равны
'string'.slice(0,3) // 'str' от start до end
'string'.slice(-2) // 'ng' начинает с конца считать
'string'.slice(-3,-2) // 'i' от -3 до -2 , если одинаково то ''

'string'.substr(0);  //'string' потму что с 0 начинается и до конца
'string'.substr(0,1); // 's' потому что от 0 и количство символов 1
'string'.substr(2,2); // 'ri' от 2 и колво символов 2
'string'.substr(2,-2); // '' так как интерпретируется в 0
'string'.substr(-2,-2); // '' интерпертируется в 0
'string'.substr(-2); // 'ng' начинает с конца

'string'.substring(0); // 'string' от 0 до конца
'string'.substring(1,1); // '' так как одинаковые методы
'string'.substring(0,1); // 's' так как от 0 до 1 
'string'.substring(1,0); // 's' сработает как от 0 до 1
'string'.substring(-1,-1); '' //отрицательные числа интерпретируются в 0 
'string'.substring(-1,1); 's' //от 0 до 1




//Сравнение происходит благодоря коду символам чем больше тем больше приоретет
 // сравнение строк у строчные буквы всегда больше заглавных
    console.log('a' > 'Z') // true
 // Буквы, имеющие диакритические знаки, идут «не по порядку»: может произойти своебразные результаты
    console.log( 'sterreich' > 'Zealand' ); // true

 // это потому что у строки кодируются в utf 16, и у каждого символа есть свой код 
 // codePointAt позволяет узнать код символа, с помощью String.fromCodePoint(code) можно получить символ
    console.log( "z".codePointAt(0) ); // 122
    console.log( "Z".codePointAt(0) ); // 90
    console.log( String.fromCodePoint(90) ); // Z
    
 // Метод правильного сравнения localeCompare озвращает число, которое показывает, 
 // какая строка больше в соответствии с правилами языка:
 // если -1 то первая строка меньше второй
 // если 1 то перва строка больше второй
 // если 0 строки равны
// localCompare принимает 3 аргумента
// 1 аргумент compareString -(обязательная строка) строка, которую нужно сравнить со строковым объектом str.
// 2 аргумент locale (необязательный) - Строка с языковой меткой либо массив таких строк, которые содержат один или несколько тегов языка
   // Если включить несколько строк языкового стандарта, нужно перечислить их в порядке убывания приоритета, чтобы первая запись была предпочитаемым языковым стандартом. Если пропустить этот параметр, то используется языковой стандарт по умолчанию
 // 3 аргумент options (необязательный) Объект, содержащий одно или несколько свойств, задающих параметры сравнения.
   // localeMatcher - Используемый алгоритм сопоставления локалей
   // usage Определяет, используется ли сравнение для сортировки или для поиска совпадающих строк. Возможными значениями являются "sort" и "search"; значением по умолчанию является "sort"
   //numeric Определяет, должно ли использоваться числовое сравнение, то есть, чтобы выполнялось условие "1" < "2" < "10"
   //caseFirst Определяет, буквы какого регистра должны идти первыми — верхнего или нижнего. Возможными значениями являются "upper", "lower", false для указания по умолчанию
    console.log( 'sterreich'.localeCompare('Zealand') ); // -1

 // также «юникодной нормализации», приводящий каждую строку к единому «нормальному» виду.
 // let s1 = 'S\u0307\u0323'; // S + точка сверху + точка снизу
    let s2 = 'S\u0323\u0307'; // S + точка снизу + точка сверху
    console.log( s1 == s2 );  // false не похожи
 // normalize() «схлопывает» последовательность из трёх символов в один: \u1e68 — S с двумя точками.
    console.log( "S\u0307\u0323".normalize() == "S\u0323\u0307".normalize() );

 // метод trim позволяет удалять пробелы с начала и сконца строки
    console.log(' Hello  '.trim()); // 'Hello'
    string.trimStart(); // "hi   "
    string.trimEnd();// "   hi"

 // метод repeat позволяет повторять строку n колво раз
    console.log('hello'.repeat(0));    // ''
    console.log('hello'.repeat(1));  // 'hellohello'

 // метод concat позволяет объединяет текст из двух или более строк и возвращает новую строку.
    const hello = 'Hello, ';
    console.log(hello.concat('Artemiy', ', good luck.')); // Hello, Artemiy, good luck

 // метод search выполняет поиск сопоставления между регулярным выражением
    console.log(~'Hello'.search('/[A-Z]/g')) // 0 - включает

 // replace позволяет заменять возвращает новую строку с некоторыми или всеми сопоставлениями с шаблоном, 
 // заменёнными на заменитель. Шаблон может быть строкой или регулярным выражением,
 // а заменитель может быть строкой или функцией, вызываемой при каждом сопоставлении.
   // заменит все вхождения так как используется g
    console.log('Hello Artemiy, and Hello Dima'.replace(/Hello/g, 'Hi'));  // Hi Artemiy, and Hi Dima
   // используется i игнорирование регистра
    console.log('Hello Artemiy, and Hello Dima'.replace(/hello/i, 'Hi'));  // Hi Artemiy, and Hello Dima
   // меняем местами слова в строке
    console.log('Artemiy Rak'.replace(/([A-Za-z]+)\s([A-Za-z]+)/, '$2 $1'));

//Метод str.search(regexp) возвращает позицию первого совпадения с regexp в строке str или -1, если совпадения нет.
//умеет возвращать только позицию первого совпадения.
   let str = "Я люблю JavaScript!";
   let regexp = /Java/;
   console.log( str.search(regexp) ); // 8

//Метод str.match(regexp) ищет совпадения с regexp в строке str. 
   //если нет флага g, то он возвращает первое совпадение в виде массива со скобочными группами и свойствами index (позиция совпадения),
   let str = "I Javalove JavaScript";  
   let result = str.match(/Java/g);
   console.log(result) //["Java", index: 2, input: "I Javalove JavaScript", groups: undefined]
   //Если у регулярного выражения есть флаг g, то он возвращает массив всех совпадений, без скобочных групп и других деталей.
   let str = "I Javalove JavaScript";  
   let result = str.match(/Java/g);
   console.log(result) //["Java", "Java"]
   //Если совпадений нет, то, вне зависимости от наличия флага g, возвращается null.
   //Это очень важный нюанс. При отсутствии совпадений возвращается не пустой массив, а именно null
   let str = "I love JavaScript";
   let result = str.match(/HTML/);
   alert(result); // null

//matchAll Он используется, в первую очередь, для поиска всех совпадений вместе со скобочными группами.
//Он возвращает не массив, а перебираемый объект с результатами, обычный массив можно сделать при помощи Array.from.
//Каждое совпадение возвращается в виде массива со скобочными группами (как str.match без флага g).
//Если совпадений нет, то возвращается не null, а пустой перебираемый объект
let str = "I Javalove JavaScript";
let result = str.matchAll(/Java/g);
console.log(Array.from(result)) 
//["Java", index: 2, input: "I Javalove JavaScript", groups: undefined]
// ["Java", index: 11, input: "I Javalove JavaScript", groups: undefined

//статические методы
//String.fromCharCode() возвращает строку, созданную из указанной последовательности значений единиц кода UTF-16.
   //Диапазон составляет от 0 до 65535 (0xFFFF). Числа больше 0xFFFF усекаются
   // работает только с самыми распространёнными символов UTF-16 , для работы со всеми используется метод ниже
   String.fromCharCode(65, 66, 67);  // "ABC"

//String.fromCodePoint() возвращает строку, созданную из указанной последовательности кодовых точек.
   String.fromCodePoint(0x1D306, 0x61, 0x1D307) // "\uD834\uDF06a\uD834\uDF07"
   String.fromCodePoint(42);       // "*"
   String.fromCodePoint(65, 90);   // "AZ"
//String.raw() татический метод String.raw() является теговой функцией для шаблонных строк; 
   //функция используется для получения необработанной строки из шаблона.
//принимает 2 аргумента 
   // 1 аргумент Правильно сформированный объект вызова, например { raw: 'string' }.
   // 2 аргумент Значения подстановок.
   //или шаблонная строка
   String.raw`templateString`

   let name = 'Боб';
   String.raw`Привет\n${name}!`; // 'Привет\nБоб!',

   String.raw({ raw: 'тест' }, 0, 1, 2); // 'т0е1с2т'

//Шаблонными литералами называются строковые литералы, допускающие использование выражений внутри. С ними вы можете использовать многострочные литералы и строковую интерполяцию.
// `строка текста`

// `строка текста 1
//  строка текста 2`

// `строка текста ${выражение} строка текста`

// tag `строка текста ${выражение} строка текста`
// преобразование строк 
  // String(), прибавить строку, alert()


//Объект Number является объектом-обёрткой, позволяющей вам работать с числовыми значениями.
   //Объект Number создаётся через конструктор Number().
   // new Number это объект обёртка
   // если занчение не может преобразоваться то становиться NaN
   // числа бывают с плавающей точкой или целым задаются они так
     const num1 = 1;
     const num2 = 1.1;
   // также можно задавать числа созначением до 2 в 53 и -2 в 53 степени
   // также мы можем укоротить запись числа с помощью e, меняя кол-во нулей 1e3 = 1 * 1000
     const num = 1e6 // 1000000
   // если мы хотим укоротить данное число 0.0001, то можно записать 1e-4 (1/10000)
     
   // Метод to.String() позволяет возвращает строковое представление числа num в системе счисления base ворьируется от 2-36
      let num = 255;

      console.log( num.toString(16) );  // ff
      console.log( num.toString(2) );   // 11111111

    // метод valueOf обычно вызывается внутренними механизмами движка 
      
      const numObj = new Number(10);
      console.log(typeof numObj); // object

     const num = numObj.valueOf();
     console.log(num);

    // если вам нужно вызвать метод непосредственно на числе нужно использовать 255..toString(16) или (255).toString(16)

    // Методы округления
    // Math.floor округляет в меньшую сторону
       console.log(Math.floor(1.9)) // 1
       console.log(Math.floor(-1.7)) // -2
       console.log(Math.floor(1.3)) // 1
       console.log(Math.floor(-1.1))  // -2

    // Math.ceil округляет в боьшую сторону
        console.log(Math.ceil(1.9)) // 2
       console.log(Math.ceil(-1.7)) // -1
       console.log(Math.ceil(1.1)) // 2
       console.log(Math.ceil(-1.9))  // -1

    // Math.trunc производит удаление дробной части не работает в IE
       console.log(Math.trunc(1.9)) // 1
       console.log(Math.trunc(-1.7)) // -1

    // Math.round округляет до ближайшего целого 
        console.log(Math.round(1.9)) // 2
       console.log(Math.round(-1.5)) // -1
       console.log(Math.round(1.3)) // 1
       console.log(Math.round(-1.6))  // -2

   // два способа есть чтобы округлить до знаков после запятой либо  toFixed(2) или Math.floor(num * 100) / 100 )
   // методя toFixed() округляет число до n знаков после запятой и возвращает строку
      console.log(1.66.toFixed(1)) // "1.7"
      console.log(1.54.toFixed(1)) // "1.5"

   // неточные вычисления в чиел в js
     // В примере снизу выдаст бесконечность так как число слишком большое, потому что оно переполниь 64 битовое 
     // хранилище, которое хранит в себе 52 бита для чисел, 11 битов для хранения положения десятичной точки, 
     // 1 бит для хранения знака
      console.log(1e500) // Infinity

     // в данном случае это происходит из за то го что дробные числа являются бесконечными в двоичной форме
      console.log(0.1 + 0.2 == 0.3) //false 0.1 + 0.2 -  0.30000000000000004

     // для решение данной проблем используется округление до ближайшего целого к примеру toFixed();
     console.log((0.1 + 0.2).toFixed(2)) // "0.30"

     // в примере ниже это происходит из-за того что для цифр 52 битов не хватает и при записи пропадают младшие разряды 
      console.log(9999999999999999) // 10000000000000000

     // есть  0 и -0 итерпретатор воспринимает одинаково
     //  Все потому, что знак представлен отдельным битом, так что, 
     // любое число может быть положительным и отрицательным, включая нуль.
  
// toString() - возвращает число в виде строки. можно указать систему счисления по умолчанию к 10.
//toLocaleString() возвращает строку с языкозависимым представлением числа.
   // аргументы locales и options позволяют приложениям определять язык, чьё поведение и соглашения по форматированию которого оно хочет использовать. 
   // locales Строка с языковой меткой 
   // options -
      //style Используемый стиль форматирования. Возможными значениями являются "decimal" для простого форматирования числа, "currency" для форматирования валюты и "percent" для форматирования процентов; значением по умолчанию является "decimal".
      //currency - Валюта, используемая при форматировании валют. "USD", "EUR"
      (1234.56).toLocaleString('en-US', {style: 'currency',currency: 'USD'}); //"$1,234.56"
      // currencyDisplay Определяет, как отображать валюту при форматировании валют. Возможными значениями являются "symbol" для использования локализованного символа валюты, например € для евро, "code" для использования кода валюты
      (1234.56).toLocaleString('en-US', {style: 'currency',currency: 'USD',currencyDisplay:'symbol'}); "$1,234.56"
      (1234.56).toLocaleString('en-US', {style: 'currency',currency: 'USD',currencyDisplay:'symbol'}); "USD 1,234.56"
// valueOf() - возвращает число в виде числа.
// toFixed округляет до n занков после запятой. и возвращает строку.
// toExponental - возвращает строку с округленным числом и записью с использованием экспоненциальной нотации.
// Параметр определяет количество знаков за десятичной запятой
// Параметр является необязательным. Если вы не укажете его, JavaScript не будет округлять число.

let x = 9.656;
x..toExponential(2); // returns 9.66e+0
x..toExponential(4); // returns 9.6560e+0

let x = 9.656;
x.toFixed(0); // returns 10
x.toFixed(2); // returns 9.66
x.toFixed(4); // returns 9.6560
// 1.35.toFixed(1) // 1.4 тут точность работает на увеличения
//6.35.toFixed(1) // 6.3 почему не 6.4 та как дробная точность большая и она 6.3549 работает в уменьшение чтобы исправить мы должны использовать math.round 
//Math.round(6.35*10)/10

//Метод toPrecision() возвращает строку, представляющую объект Number с указанной точностью.
let x = 9.656;
x.toPrecision(); // returns 9.656
x.toPrecision(2); // returns 9.7 округление сработало
x.toPrecision(4); // returns 9.656

//статические свойства
Number отличие от parseInt от parseFloat, тем что если будет 12eda, то они обрежут до 12
parseInt от parseFloat отличие дробные в parseFloat и использование 1e3, и с плавуещей запятой
если будет в parseFloat 12.21.3 то выведет 12.21
// Number.parseInt() от parseInt() ничем не отличается
// Функция parseInt() принимает строку в качестве аргумента и возвращает целое число

// Метод Number.parseInt() разбирает строковый аргумент и возвращает целое число.
// Этот метод ведёт себя идентично глобальной функции parseInt() и является частью

// Свойство Number.MAX_VALUE представляет максимальное числовое значение, представимое в JavaScript.
// Свойство Number.MIN_VALUE представляет минимальное положительное числовое значение, представимое в JavaScript.

// Константа Number.MAX_SAFE_INTEGER представляет максимальное безопасное целочисленное значение в JavaScript (253 - 1).
// Смысл этого числа в том, что в JavaScript используется формат чисел с плавающей запятой двойной точности,
// а он может безопасно представлять числа только в диапазоне от -(253 - 1) до 253 - 1.

// Свойство Number.NaN представляет «не число». Эквивалентно глобальному объекту NaN.
// Глобальное свойство NaN является значением, представляющим не-число (Not-A-Number).

// Свойство Number.NEGATIVE_INFINITY представляет значение отрицательной бесконечности.
// Свойство Number.POSITIVE_INFINITY представляет значение положительной бесконечности.

// Метод Number.isInteger() определяет, является ли переданное значение целым числом. возвращаяя true/false
// Метод Number.isFinite() определяет, является ли переданное значение конечным числом.
//Number.isFinite () отличается от глобальной функции isFinite (). Глобальная функция isFinite () преобразует проверенное значение в число, а затем проверяет его. Number.isFinite () не преобразует значения в Number и не возвращает true для любого значения, которое не относится к типу Number.
// Метод Number.isNaN() определяет, является ли переданное значение NaN. возвращает true только для числовых значений
// Это более надёжная версия оригинальной глобальной функции isNaN();
//В отличие от глобальной функции isNaN(), Number.isNaN() не имеет проблемы принудительного преобразования параметра в число. Это значит, что в него безопасно передавать значения, которые обычно превращаются в NaN, но на самом деле NaN не являются.

Number.isNaN(NaN); // true
Number.isNaN(Number.NaN); // true
Number.isNaN(0 / 0) // true

// При использовании глобальной функции isNaN() это всё будет true
Number.isNaN('NaN');      // false
Number.isNaN(undefined);  // false
Number.isNaN({});         // false
Number.isNaN('blabla');   // false

// А это всё в любом случае будет false
Number.isNaN(true);
Number.isNaN(null);
Number.isNaN(37);
Number.isNaN('37');
Number.isNaN('37.37');
Number.isNaN('');
Number.isNaN(' ');

//Number.isFinite отличается тем что он пытается не пытается преобразовать и принимает только числа преобразовать в число, а isFinite пытается преобразовать,но NaN,infinity,-infinity,undefinedбудет false,а null true и строка
     //для проверки специальных числовых значений(Infinity,-Infinity,NaN) есть функции isNaN и isFinite
     // isNaN преобразует значение в число и проверяет является ли оно NaN, также NaN не является равным ничему другому 
     // даже самому себе
       console.log(isNaN(NaN)) // true
       console.log(isNaN("str")) //true
     // isFinitу преобразует аргумент в число и возвращает true, если оно является обычным числом, 
     //т.е. не NaN/Infinity/-Infinity:
       console.log( isFinite("15") ); // true
       console.log( isFinite("str") ); // false потому что специальноезначение NaN
       console.log( isFinite(Infinity) ); // false потому что специальное значение Infinity
       console.log( isFinite('') ); // 0 так как интерпритируется в 0 

      //метод Object.is, который сравнивает значения примерно как ===
       console.log( Object.is(NaN, NaN)) //true
       console.log(Object.is(0, -0)) // false разные значения если сравнивать

      // есть функции которые позволяют преобразовать из строки в число это parseInt и parseFloat

      // parseInt принимает строку в качестве аргумента и возвращает 
      // целое число в соответствии с указанным основанием системы счисления. Дробную часть откинет и вернет целое
         console.log( parseInt('100px') ); // 100
         console.log( parseInt('12.3') ); // 12, вернётся только целая часть
         console.log( parseInt('0xff', 16) ); // 255
         console.log( parseInt('a233') ); // NaN так как на первом сиволе произойдет остановка чтения

      // parseFloat -  принимает строку в качестве аргумента и возвращает десятичное
         console.log( parseFloat('12.5em') ); // 12.5
         console.log( parseInt('a233') ); // NaN так как на первом сиволе произойдет остановка чтения
         console.log( parseFloat('12.3.4') ); // 12.3, произойдёт остановка чтения на второй точке


    // другие математические функции Math.random(), Math.min(), Math.max(), Math.pow()
      // Math.random - возвращает случайное число от 0 до 1
         console.log( Math.random() ); // 0.1234567894322

      // Math.min и Math.max Возвращает наибольшее/наименьшее число из перечисленных аргументов.
         console.log( Math.max(3, 5, -10, 0, 1) ); // 5
         console.log( Math.min(1, 2) ); // 1

      // Math.pow(n, power) Возвращает число n, возведённое в степень power
         console.log( Math.pow(2, 10) ); // 2 в степени 10 = 1024

      var d = new Date('December 17, 1995 03:24:00');
      console.log(Number(d)); //"819199440000".

  // для преобразования к числу, +,Number, parseFloat, parseInt

// Symbol - представляет собой уникальный идентификатор.
игнорируется for in и json.stringify
Reflect.ownKeys(obj) для получения символов
//Symbol) — это уникальный и неизменяемый тип данных, который может быть использован как идентификатор для свойств объектов. Символьный объект (анг. symbol object) — это объект-обёртка (англ. wrapper) для примитивного символьного типа.
// Позволяет избежать повторений этих самых ключей если вы расширяете существующий объект не беспокоясь об перезаписывании
// благодоря символу используется интератор для объектов for of
   //символ нельзя преобразовать к строке или числу. ошибка TypeError
//использующий функцию Symbol(), не создаст глобальный символ, который был бы доступен в любом месте вашего кода
    // способ создания символа
    Допустим, требуется установить идентификатор для объекта. Если просто задать ключ из двух букв строчного типа (id), существует огромный риск того, что несколько библиотек будут использовать один и тот же ключ.
function lib1tag(obj) {
  obj.id = 42;
}
function lib2tag(obj) {
  obj.id = 369;
}
Благодаря символам каждая библиотека может генерировать необходимые ей символы при создании экземпляра. Далее эти символы можно использовать для назначения свойств объектам.

const library1property = Symbol('lib1');
function lib1tag(obj) {
  obj[library1property] = 42;
}
const library2property = Symbol('lib2');
function lib2tag(obj) {
  obj[library2property] = 369;
}

     let id1 = Symbol('id'); //внутри символа есть описание которое не обязательное
     let id2 = Symbol('id');

     console.log(id1 == id2) //false
     //new Symbol() -- будет ошибка , потому что не поддерживает явное создание объектов примитивов
     //если нужно обернуть то только так Object( symbol )
   // символы автоматически не преобразовываются в строку, чтобы преобразовать нужно использовать toString()
     console.log(Symbol('id').toString()); //Symbol(id)
   // для того чтобы отобразить описание только нужно использовать description
     console.log(Symbol('id').description); //id
   
   // Символы позволяют создавать «скрытые» свойства объектов,
   // к которым нельзя нечаянно обратиться и перезаписать их из других частей программы.
      let id = Symbol("id");
      let user = {name:"Artem", [id]:"Индификатор"}
     //id квадратных скобках, чтобы оно не интерпретировалось как строка, а было получено в результате вычисления выражения.
      Object.defineProperty( user, 'role', { //для объекта мы должны использовать defineProperty удобнее использовать Symbol вместо этой конструкции
        enumerable: false,
        value: 'admin'
    });
   // но все равно к нему можно обратиться на прямую user.role ->> 'admin'
   //если так сделать у символа то будет undefined, нужно обратиться к свойству по ссылке на символ user[role]


      user.id = "Их идентификатор";
      console.log(user) // {name:'Artem',  [id]:"Индификатор"}

  // Символ игнорируется перебором for in и Object.keys(user) тоже игнорируется
 // игнорируется JSON.stringify({[Symbol("foo")]: "foo"}); {}
    let id = Symbol("id");
    let user = {
      name: "Вася",
      [id]: 123
    };

    for (let key in user) console.log(key); // name

  //  Object.assign - позволяет копировать свойства
     let id = Symbol("id");
     let user = {
       [id]: 123
     };

     let clone = Object.assign({}, user);
     console.log(clone[id]) // 123

Методы Symbol (Symbol.forKey, Symbol.for )

    Symbol.for(key)
// Ищет существующие символы по заданному ключу и возвращает его, если он найден. В противном случае создаётся новый символ для данного ключа в глобальном реестре символов.
  // глобальные символы, это чтобы символы с одинаковыми именами были одной сущностью
  // будет доступен в любом месте вашего кода.
    // глобальный реестр и, при наличии в нём символа с именем key, 
    // возвращает его, иначе же создаётся новый символ Symbol(key) и записывается в реестр под ключом key.
    
    let id1 = Symbol.for("id");
    let id2 = Symbol.for("id");
    console.log(id1 === id2) // true

    Symbol.forKey 
    // принимает в качестве аргумента глобальный символ и возвращает его имя, для не глобальных
    // символов этот метод не используется будет возвращать undefined, для не глобальных используется description
      let sym1 = Symbol.for("id");
      console.log(Symbol.keyFor(sym)) // id

Метод Object.getOwnPropertySymbols() возвращает массив всех символьных свойств, найденных непосредственно на переданном объекте.
// var obj = {};
// var a = Symbol('a');
// var b = Symbol.for('b');

// obj[a] = 'localSymbol';
// obj[b] = 'globalSymbol';

// var objectSymbols = Object.getOwnPropertySymbols(obj);

// console.log(objectSymbols.length); // 2
// console.log(objectSymbols);   

     // Встроенные методы Symbol

     // Symbol.hasInstance — который используется для определения является ли объект экземпляром конструктора.
        class MyArray {  
           static [Symbol.hasInstance](instance) {
           return Array.isArray(instance);
           }
        }
        console.log([] instanceof MyArray); // true

 // Symbol.iterator Этот метод вызывается, когда мы хотим вернуть итератор для 
       // оператора распространения или цикла for...of 
       // Мы можем создавать собственные итерируемые объекты,
       let range = {
    from:1,
    to:5,
    };
range[Symbol.iterator] = function() {
        let cur = this.from;
        let last = this.to;

        return {
               next(){
                    if(cur <= last){
                        return {
                            done:false,
                            value:cur++
                        };
                    } else {
                         return {
                            done:true
                        
                        };
                    }
                }
        }
    }

for(let num of range){
    console.log(num);
}
       const rangeIterator = '0123456789'[Symbol.iterator]();
console.log(rangeIterator.next()); // {value: "0", done: false}
...
console.log(rangeIterator.next()); // {value: "9", done: false}
console.log(rangeIterator.next()); // {done: true}

       var myIterable = {}
       myIterable[Symbol.iterator] = function* () {
           yield 1;
           yield 2;
           yield 3;
       };
       [...myIterable] // [1, 2, 3]
  
// Symbol.match определяющий соответствие строки регулярному выражению.
      //Этот метод вызывается функцией String.prototype.match().
        var re = /foo/;
        re[Symbol.match] = false; // устанавливает что, знение не является регулярным выражением
        "/foo/".startsWith(re); // true
        "/baz/".endsWith(re); // false 

      // Symbol.replace — задающий метод для замены подстрок в строке.
      //функция вызывается методом String.prototype.replace().
         class Replacer {
             constructor(value) {
               this.value = value;
             }
          [Symbol.replace](string) {
          return this.value.replace(string, this.value);
            }
         }
        console.log('foo'.replace(new Replacer('bar'))); //bar
      // Symbol.search указывает метод, который вернёт индекс внутри строки, соответствующей регулярному выражению.
       //вызываеться методом String.prototype.search().
      class Searcher {
          constructor(value) {
             this.value = value;
          }
          [Symbol.search](string) {
           return string.indexOf(this.value) != -1;
          }
       }
       console.log('foobar'.search(new Searcher('bar'))); //true

    // Symbol.isConcatSpreadable является логическим значением, которое указывает, 
    // должен ли объект быть сведен в массив методом concat.
       let alpha = ['a', 'b', 'c'], 
       numeric = [1, 2, 3]; 
 
       let alphaNumeric = alpha.concat(numeric); 
       console.log(alphaNumeric); // ['a', 'b', 'c', 1, 2, 3 ]
      
       numeric[Symbol.isConcatSpreadable] = false;
       let alphaNumeric = alpha.concat(numeric); 
       console.log(alphaNumeric); // ['a', 'b', 'c', [1, 2, 3] ]

var x = [1, 2, 3];

var fakeArray = {
  [Symbol.isConcatSpreadable]: true,
  length: 2,
  0: 'hello',
  1: 'world'
}

x.concat(fakeArray);
      

      // Symbol.species — позволяющий определить конструктор, использующийся для создания порождённых объектов.
      //  при использовании метода вроде map(), использующего конструктор по умолчанию, 
      //  вам может потребоваться возвращать объект родительского типа Array, вместо MyArray
        class MyArray extends Array {
          // Перегружаем species для использования родительского конструктора Array
           static get [Symbol.species]() { return Array; }
        }
        let  a = new MyArray(1,2,3);
        let mapped = a.map(x => x * x);

       console.log(mapped instanceof MyArray); // false
       console.log(mapped instanceof Array);   // true

      // Symbol.split определяет метод, который делит строки в индексах, которые соответствуют регулярному выражению. 
     // Эта функция вызывается методом String.prototype.split().
        class Split1 {
  
            constructor(value) {
    
            this.value = value;
  
            }
  
           [Symbol.split](string) {
 
              const index = string.indexOf(this.value);
          
    return `${this.value}${string.substr(0, index)}/${string.substr(index + this.value.length)}`;

           }

         }
        

console.log('foobar'.split(new Split1('foo'))); //"foo/bar"
      //  Symbol.toPrimitive Метод, который преобразует объект в соответствующее примитивное значение. 
      // Он вызывается, когда используется унарный оператор + или конвертирует объект в примитивную строку.
        let obj = {
          [Symbol.toPrimitive](hint) {
               if (hint == 'number') {
                  return 10;
               }   
               if (hint == 'string') {
                  return 'hello';
               }
               if (hint == 'true') {
                   return true;
               }
               if (hint == 'false') {
               return false;
               }
               return true;
               }
          };
         console.log(+obj);     
         console.log(`${obj}`); 
         console.log(!!obj);
         console.log(!obj);
        // Symbol.toString Метод, который возвращает строковое представление объекта.
      

// new Date() - Встроенный объект котрый содержит дату и время и предстовляет методы управления между ними

// Если никаких аргументов передано не было, конструктор создаёт объект Date  для текущих даты и времени, согласно системным настройкам.
// День содержит 86 400 000 миллисекунд. Диапазон дат объекта Date варьируется от -100 000 000 до 100 000 000 дней относительно 1 января 1970 года по UTC.
   // new Date(milliseconds)
   // Создать объект Date с временем, равным количеству миллисекунд (тысячная доля секунды), 
   // прошедших с 1 января 1970 года 

     // 0 соответствует 01.01.1970 UTC+0
       let Jan01_1970 = new Date(0);
       console.log( Jan01_1970 );

   // теперь добавим 24 часа и получим 02.01.1970 UTC+0

      let Jan02_1970 = new Date(24 * 3600 * 1000);
      conole.log( Jan02_1970 );

   // Целое число, представляющее собой количество миллисекунд, прошедших с начала 1970 года, называется таймстамп

   // 31 декабря 1969 года
      let Dec31_1969 = new Date(-24 * 3600 * 1000);
      console.log( Dec31_1969 ); 

   //  если это строка и он один прочитывается дата, если единственный параметр
    let date = new Date("2017-01-26"); // Thu Jan 26 2017 02:00:00 GMT+0200 (Восточная Европа, стандартное время)
   // Создать объект Date с заданными компонентами в местном часовом поясе. Обязательны только первые два аргумента.
   // new Date(year, month, date, hours, minutes, seconds, ms)

   new Date(2011, 0, 1, 0, 0, 0, 0); // // 1 Jan 2011, 00:00:00

   // Для получения компонентов даты, в вашем часовоем поясе
      getFullYear() // Получить год (4 цифры)
      getMonth() // Получить месяц, от 0 до 11.
      getDate() // Получить день месяца, от 1 до 31, что несколько противоречит названию метода.
      getHours(), getMinutes(), getSeconds(), getMilliseconds() 
      // Получить, соответственно, часы, минуты, секунды или миллисекунды.
      getDay() // Вернуть день недели от 0 (воскресенье) до 6 (суббота). 
      getTime() // возвращает таймстамп количество миллисекунды
      getTimezoneOffset() // Возвращает разницу в минутах между местным часовым поясом и UTC:
     // Однако существуют и их UTC-варианты, 
     // возвращающие день, месяц, год для временной зоны UTC+0: getUTCFullYear(), getUTCMonth(), getUTCDay().
    // текущая дата
     let date = new Date();  // час в вашем текущем часовом поясе

    console.log( date.getHours() );

     // час в часовом поясе UTC+0 (лондонское время без перехода на летнее время)

     console.log( date.getUTCHours() );

  // Следующие методы позволяют установить компоненты даты и времени:
       setFullYear(year, [month], [date])
       setMonth(month, [date])
       setDate(date)
       setHours(hour, [min], [sec], [ms])
       setMinutes(min, [sec], [ms])
       setSeconds(sec, [ms])
       setMilliseconds(ms)
       setTime(milliseconds)// (устанавливает дату в виде целого количества миллисекунд, прошедших с 01.01.1970 UTC)
   // У всех этих методов, кроме setTime(), есть UTC-вариант, например: setUTCHours().

     let today = new Date(); 
     today.setHours(0);
     console.log(today);

   // в  Date происходит автоисправление если не правильно указал дату

     let date = new Date(2013, 0, 32); // 32 Jan 2013 ?!?
     console.log(date);

   // нам не нужно беспокоится о том что високоснеый этот год или нет Data сама вычеслит

     let date = new Date(2016, 1, 28);
        date.setDate(date.getDate() + 2);
     console.log( date ); // 1 Mar 2016

  // чтобы получить дату по прошествии заданного отрезка времени 

    let date = new Date();
    date.setSeconds(date.getSeconds() + 70);

     console.log( date );
   // Также можно установить нулевые или отрицательные значения

   // Преобразование к числу, разность дат только вычисляется в миллисекундах, date2 - date1
 
   let date = new Date();
   console.log(+date); // выведет в миллисекундах, можно также использовать getTime()

  // Date.now()  возвращающий текущую метку времени.

   // Семантически он эквивалентен new Date().getTime(), однако метод не создаёт промежуточный объект Date. 
   // Так что этот способ работает быстрее и не нагружает сборщик мусора.


  // Бенчмаркинг - это способ которые позволяет замерить производительность работы (методов, функций) 

var d = new Date(2018); -Вы не можете пропустить месяц. Если указать только один параметр, он будет обрабатываться как миллисекунды.
var d = new Date(99, 11, 24); Один и два цифр лет будет интерпретироваться как 19xx:

можно указывать в отрицательные данные в миллисекунды, но это датбудут вычитаться от 1970 года
//способы создание даты
//new Date() // Sun Nov 08 2020 15:41:01 GMT+0200 (Восточная Европа, стандартное время) {} в объектном виде выведет
// Date() // "Sun Nov 08 2020 15:41:01 GMT+0200 (Восточная Европа, стандартное время)" строкой
new Date(2020) //Thu Jan 01 1970 03:00:02 GMT+0300 (Восточная Европа, стандартное время){}
new Date(2020,02) // Sun Mar 01 2020 00:00:00 GMT+0200 (Восточная Европа, стандартное время) {}
new Date(2020,02,05) // Thu Mar 05 2020 00:00:00 GMT+0200 (Восточная Европа, стандартное время) {}
//и тд со временем
//так же через setFullYear() и др. можно указать нужный вам год в соответствии стандарту

методы Даты
   Статические методы  и свойства
      Date.length 
         //Значение свойства Date.length равно 7. Это количество аргументов, обрабатываемых конструктором.
         //Свойства, унаследованные из Function: arity, caller, constructor (en-US), length, name
      Date.parse()
      // разбирает строковое представление даты и возвращает количество миллисекунд, прошедших с 1 января 1970 года 00:00:00 по UTC.
      // Формат строки должен быть следующим: YYYY-MM-DDTHH:mm:ss.sssZ, где:
    // YYYY-MM-DD – это дата: год-месяц-день.
    // Символ "T" используется в качестве разделителя.
    // HH:mm:ss.sss – время: часы, минуты, секунды и миллисекунды.
    // Необязательная часть 'Z' обозначает часовой пояс в формате +-hh:mm. Если указать просто букву Z, то получим UTC+0.
    //часовой пояс указывается
    // Возможны и более короткие варианты, например, YYYY-MM-DD или YYYY-MM, или даже YYYY

      let ms = Date.parse('2012-01-26T13:51:50.417+07:00');  
      let ms = Date.parse('2012-01-26T13:51:50.417Z');
      Date.parse('Wed, 09 Aug 1995 00:00:00 GMT');
      Date.parse('Thu, 01 Jan 1970 00:00:00');  
      console.log(ms); // 1327611110417

Метод Date.now() возвращает количество миллисекунд, прошедших с 1 января 1970 года 00:00:00 по UTC
   Date.now();
Метод Date.UTC() принимает те же самые параметры, что и самая длинная форма конструктора Date и возвращает количество миллисекунд, прошедших с 1 января 1970 года 00:00:00 по UTC.
    Метод UTC() отличается от конструктора Date двумя вещами.
   Метод Date.UTC() использует всемирное время вместо местного времени.
   Метод Date.UTC() возвращает значение времени как число вместо создания объекта Date.
      new Date(Date.UTC(2018, 11, 1, 0, 0, 0)); //Sat Dec 01 2018 02:00:00 GMT+0200 (Восточная Европа, стандартное время)

Методы экзэмпляра

toString () //переводит в строку дату Thu Mar 11 2021 15:29:19 GMT+0300 (Москва, стандартное время)
Метод toUTCString () //преобразует дату в строку UTC (стандарт отображения даты).Thu, 11 Mar 2021 12:29:07 GMT
Метод toDateString ()// преобразует дату в более удобочитаемый формат: Thu Mar 11 2021
Метод toISOString() возвращает строку в формате ISO Z- обозначает местный формат времени тоесть с комп настройками
   var today = new Date('05 October 2011 14:48 UTC');
   console.log(today.toISOString()); // вернёт 2011-10-05T14:48:00.000Z4

Метод toJSON() возвращает представление объекта Date в виде JSON.
Вызов метода toJSON() вернёт строку, отформатированную в JSON (при помощи метода toISOString())
new Date().toJSON() //"2021-03-22T18:28:06.880Z"

Метод toTimeString() возвращает часть, содержащую только время объекта 
   new Date().toTimeString() //"20:29:35 GMT+0200 (Восточная Европа, стандартное время)"

Метод toUTCString() преобразует дату в строку, используя часовой пояс UTC.
new Date().toUTCString()  //"Mon, 22 Mar 2021 18:30:42 GMT"

Метод toLocaleString() возвращает строку с языкозависимым представлением даты .
аргументы locales и options позволяют приложениям определять язык, чьи соглашения по форматированию должны использоватьс
   //LOCALES  "en-en","ru-ru"
   // options timeZone Используемый часовой пояс. Единственным значением, которые реализации обязаны распознавать, является "UTC"; значением по умолчанию является часовой пояс по умолчанию среды выполнения
   //{timeZone:'Europe/London'}(часовой пояс) // "Asia/Shanghai", "Asia/Kolkata" или "America/New_York"
  // hour12 Определяет, использовать ли 12-часовой формат времени  true или false
  //timeZoneName - short или long -> GMT или время по гринвичу
  //year,month, day, weekday,hour,secons, minute
  var event = new Date();
console.log(event.toLocaleString('en-GB', { timeZone: 'Europe/London' ,formatMatcher:"basic",weekday:"long" })); // указывает время и дату с timezone(часовой пояс) 10/08/2021, 10:46:31
 var event = new Date();
console.log(event.toLocaleTimeString('en-GB', { timeZone: 'Europe/London'})); // указывает время с timezone(часовой пояс)  10:46:31
var event = new Date();
console.log(event.toLocaleTimeString('en-GB', { timeZone: 'Europe/London'})); // указывает дата с timezone(часовой пояс)  10:46:31


Метод toLocaleTimeString() возвращает строку с языкозависимым представлением части со временем в этой дате.
Метод toLocaleDateString() возвращает строку с языкозависимым представлением части с датой в этой дате.
  Дата ISO "2015-03-25" (Международный стандарт)
Короткая Дата "03/25/2015"
Длинная Дата "Mar 25 2015" or "25 Mar 2015"

var d = new Date("2015-03-25"); //Wed Mar 25 2015 03:00:00 GMT+0300 (Москва, стандартное время)
var d = new Date("2015-03"); //Sun Mar 01 2015 03:00:00 GMT+0300 (Москва, стандартное время)
var d = new Date("2015"); //Thu Jan 01 2015 03:00:00 GMT+0300 (Москва, стандартное время)
var d = new Date("2015-03-25T12:00:00Z"); //Wed Mar 25 2015 15:00:00 GMT+0300 (Москва, стандартное время)

//игнорируються запятые
var d = new Date("JANUARY, 25, 2015");
var d = new Date("January 25 2015");
var d = new Date("Jan 25 2015");

var d = new Date("Mar 25 2015"); //обычно пишутся mm dd yyyy Wed Mar 25 2015 00:00:00 GMT+0300 (Москва, стандартное время)
var d = new Date("25 Mar 2015");//Месяц и день могут быть в любом порядке: Wed Mar 25 2015 00:00:00 GMT+0300 (Москва, стандартное время)

указание time zone ('2015-05-12 12:00:00 GMT+08');
new Date("2015-05-12 12:00:00 GMT+02"); //Tue May 12 2015 13:00:00 GMT+0300 (Москва, стандартное время) {} от

new Date("Mar 31 1950") > 0 //не правильно так как отрицательное число(потому что что перед 1970 1 января идут отрицательные миллесекунды)

задаача на получения день недели в короткой формате

function getWeekDay(date) {
  let days = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];

  return days[date.getDay()];
}

let date = new Date(2014, 0, 3); // 3 января 2014 года
alert( getWeekDay(date) ); //ПТ

// узнать каккой день месяца был несколько дней назад

function getDateAgo(date, days) {
  let dateCopy = new Date(date);

  dateCopy.setDate(date.getDate() - days);
  return dateCopy.getDate();
}

let date = new Date(2015, 0, 2);

alert( getDateAgo(date, 1) ); // 1, (1 Jan 2015)
alert( getDateAgo(date, 2) ); // 31, (31 Dec 2014)
alert( getDateAgo(date, 365) ); // 2, (2 Jan 2014)

//последнее число месяца
function getLastDayOfMonth(year, month) {
  let date = new Date(year, month + 1, 0);
  return date.getDate();
}

alert( getLastDayOfMonth(2012, 0) ); // 31
alert( getLastDayOfMonth(2012, 1) ); // 29
alert( getLastDayOfMonth(2013, 1) ); // 28

//Сколько сегодня прошло секунд?
function getSecondsToday() {
  let now = new Date();

  // создаём объект с текущими днём/месяцем/годом
  let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

  let diff = now - today; // разница в миллисекундах
  return Math.round(diff / 1000); // получаем секунды
}

alert( getSecondsToday() );

//альтернатива
function getSecondsToday() {
  let d = new Date();
  return d.getHours() * 3600 + d.getMinutes() * 60 + d.getSeconds();
}

//сколько сеунд осталось до завтра
function getSecondsToTomorrow() {
  let now = new Date();

  // завтрашняя дата
  let tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate()+1);

  let diff = tomorrow - now; // разница в миллисекундах
  return Math.round(diff / 1000); // преобразуем в секунды
}
