// Лексическое окружение — это определенная структура, которая используется для определения связи
// Идентификаторов (имен) с конкретными переменными и функциями на основе вложенности (цепочки)
// лексических областей видимости.

//Лексическое окружение - это скрытый объект, который связан с функцией и создаётся при её запуске.
// В нём находятся все локальные переменные этой функции, ссылка на внешнее лексическое окружение,
// а также некоторая другая информация

// globalEnvironment
// outer: null — нет родительского окружения
// environmentRecord = {x: 10, foo: function foo(){...}}

var x = 10;

function foo() {
    // fooEnvironment
    // outer: globalEnvironment — глобальное окружение
    // environmentRecord = {y: 20, bar: function bar(){...}}

    var y = 20;

    function bar() {
        // barEnvironment
        // outer: fooEnvironment — глобальное окружение
        // environmentRecord = {z: 30}

        var z = y + x; // переменная y найдена в fooEnvironment, а переменная x в globalEnvironment
    }
    bar();
}
foo();

// после того как функция выполнилась лексическое окружения функции foo, bar стираются сборщиком мусора,

// globalEnvironment
// outer: null — нет родительского окружения                     //ссылка на анонимную функцию
// environmentRecord = {makeCounter: function makeCounte(){...},counter:function (){...}}

function makeCounter() {
    // outer: globalEnvironment — глобальное окружение
    // environmentRecord = {count: 0,  function (){ return count++;}}

    let count = 0;
    return function() { // как  выполниться функция лексическое окружение этой функции удалиться
        return count++; // outer: makeCounterEnvironment — глобальное окружение
        // environmentRecord = { }
    };
}

let counter = makeCounter();

// из за того есть ссылка между глобальным и лексическим окружением makeCountre,
//  лексическое окружение не удаляется
console.log(counter()); // 0
console.log(counter()); //1

// 1. Лексический анализатор дойдет до строки функции showValue
// console.log("Value from showValue: " + value);
// в которой запрашивается переменная по идентификатору value
//2. Лексический анализатор начнет поиск переменной, начиная с текущей области видимости, в функции showValue.
//3. Не найдя переменную в локальной области видимости, лексический анализатор обратится 
//к окружению функции showValue и найдет её в родительской области видимости, где такая переменная объявлена.
// 4. найдет в глобальном и выведет value = 2.

//Поэтому несмотря на то, что showValue вызывается из функции wrapper, где объявлена своя переменная value, 
//функция showValue выведет значение той переменной, которая была зафиксирована во время лексического анализа,
// то есть число 2.

let value = 2;

function showValue() {
    console.log("Value from showValue: " + value); // 2  
}

function wrapper() {
    let value = 3;
    console.log("Value from wrapper: " + value); // 3

    showValue();
}
wrapper();

let value = 100;

function worker() {
    // environmentRecord: {value:  function value() {}}
    //перезапишет на
    // environmentRecord: {value: 10}
    value = 10;

    return; // завершает выполнение функции и возвращает значение undefined
    function value() {}
}
worker(); // останеться в области worker {value: 10} вернет undefined так как ничего не возвращает;
console.log(value); //100 так как запрашивается глобальная переменная


function outer() {
    // первое всплывшее объявление добавит идентификатор inner в запись окружения — environmentRecord: {inner: function inner(){ return 3;}}
    // вторым всплывшим объявлением его значение перезапишется — environmentRecord: {inner: function inner(){ return 8;}}

    function inner() {
        return 3;
    }

    return inner(); // результат выполнения функции inner() и будет возвращаемым результатом выполнения функции outer()

    function inner() {
        return 8;
    }
}

let result = outer();
console.log(result); // 8

function parent() {
    // первое всплывшее объявление переменной добавит идентификатор hoisted со значением undefined в запись окружения — environmentRecord: {hoisted: undefined}
    // вторым всплывшим объявлением его значение перезапишется — environmentRecord: {hoisted: function hoisted(){ return "Я функция!";}}

    var hoisted = "Я переменная!";

    function hoisted() {
        return "Я функция!";
    }

    return hoisted(); // результат выполнения функции hoisted() и будет возвращаемым результатом выполнения функции parent()
}

let result = parent();
console.log(result); // typeError hoisted is not a function

// После этапа создания контекста начнется этап выполнения и выполнение строки var hoisted = "Я переменная!"; 
//снова изменит значение идентификатора hoisted на "Я переменная!". 
//Поэтому вызов hoisted() как функции после ключевого слова return вызовет ошибку TypeError, 
//так как идентификатор hoisted уже указывает не на функцию, а на строку.

let result = outer();
console.log(result); //3

function outer() {
    // на этапе создания {inner: undefined} 2 раза всплывает а
    // потом на этапе выплнения присваивается
    // environmentRecord: {inner: function() {return 3;}; }
    var inner = function() {
        return 3;
    }; // <-- выполнение это строки изменило значение inner в записи окружения

    return inner();

    var inner = function() {
        return 8;
    };
}

var value = 10;

let worker = function() {
    console.log("Первое значение: " + value); // та как на этапе создание всплывает со значение гтвуаштув
    var value = 20;
    console.log("Второе значение: " + value); //20 // перезаписалось на этапе выполнения
};

worker();
console.log("Третье значение: " + value); //10


// Замыкание это функция вместе со всеми внешними переменными, которые ей доступны.

// Замыкания позволяют реализовывать аналог приватных переменных,
// минимизировать выход переменных в глобальную область видимости.

// Для чего нужно замыкание - чтобы функцию можно было использовать 
//как объект первого порядка(функции высшего порядка)

//Функция высшего порядка — это функция, которая может принимать другую функцию в качестве аргумента 
// или возвращать другую функцию в качестве результата. (map,filter,reduce)
const arr1 = [1, 2, 3];
const arr2 = arr1.map(item => item * 2);

console.log(arr2);

const strArray = ['JavaScript', 'Python', 'PHP', 'Java', 'C'];

function mapForEach(arr, fn) {
    const newArray = [];
    for (let i = 0; i < arr.length; i++) {
        newArray.push(
            fn(arr[i])
        );
    }

    return newArray;
}

const lenArray = mapForEach(strArray, function(item) {
    return item.length;
});
console.log(lenArray) //10, 6, 3, 4, 1

//Для чего нужно замыкание -  нужно для того чтобы локальную переменную после завершения
// использовать при запуске n каличество раз