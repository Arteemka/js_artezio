//Для чего нам модули
// абстрагировать код, передавая функциональные возможности сторонним библиотекам, 
// так что нам не придётся разбираться во всех сложностях их реализации;
// инкапсулировать код, скрывая его внутри модуля, если не хотим, чтобы его изменяли;
// переиспользовать код, избавляясь от необходимости писать одно и то же снова и снова;
// управлять зависимостями, легко изменяя зависимости без необходимости переписывать наш код.
 
//Модуль – это просто файл. Один скрипт – это один модуль. у модуля всегда используется 'usestrict'

// Паттерн Модуль
 //поскольку JavaScript не поддерживает классы нативно
 //Модуль — часть кода, которая инкапсулирует детали реализации и предоставляет открытый API для использования другим кодом.
 //Шаблон «Модуль» основывается на немедленно вызываемой функции (IIFE).
 //Немедленно вызываемая функция образует локальную область видимости,
 // в которой можно объявить необходимые приватные свойства и методы. 
 //Результат функции — объект, содержащий публичные свойства и методы.
 //С помощью передачи параметров в немедленно вызываемую функцию можно импортировать зависимости в модуль. 
 //Это увеличивает скорость разрешения переменных, поскольку импортированные значения становятся локальными 
 //переменными внутри функции.
 //позволяет скрывать приватную информацию в замыканиях, давая доступ лишь к тому, что решил сделать 
 //общедоступным разработчик.
 //Управлять областью видимости переменных в JavaScript можно, пользуясь паттерном «Модуль».
 //Для того чтобы создать приватную область видимости, можно воспользоваться замыканием. 
 //Как известно, функции создают собственные области видимости, содержимое которых отделено от глобальной области видимости:

//Паттерн "Модуль" базируется на замыканиях и состоит из двух компонентов: внешняя функция, которая определяет лексическое окружение, и возвращаемый набор внутренних функций, которые имеют доступ к этому окружению.
let Сalculator = (function(){
        let data = { number: 0};
        function showResult (number){
            console.log("Result: ", number);
        }
        return {
            sum: function(n){
                data.number += n;
            },
            subtract: function(n){
                data.number -= n;
            },
            show: function(){
                showResult(data.number);
            }
        }
    })();
    Calculator.sum(1)
    Calculator.sum(1)
    Calculator.show()//2
// Здесь определена переменная Сalculator, которая представляет результат анонимной функции. Внутри подобной функции определен объект data с некоторыми данными.
// Сама анонимная функция возвращает объект, который определяет функцию sum, substract, show. Возвращаемый объект определяет общедоступый API, через который мы можем обращаться к данным, определенным внутри модуля.


 //С точки зрения структуры модуль CommonJS — это часть JavaScript-кода, которая экспортирует определенные переменные, 
 //объекты или функции, делая их доступными для любого зависимого кода.
 //Модули CommonJS состоят из двух частей: module.exports содержит объекты, 
 //которые модуль хочет сделать доступными, а функция require() используется для импорта других модулей.
//поддерживает объект как модули
//синхронно подключает модули
CommonJSСпецификация в основном работает на стороне сервера и загружает модули синхронно, и большинство загруженных файловых ресурсов находятся на локальном сервере, поэтому скорость выполнения или время не являются проблемой.
Основные команды функции модуля:require  module.exports require
Через примеры мы можем видеть require(moduleId)Чтобы загрузить содержимое других модулей, возвращаемое значение - это API, предоставляемый внешним модулем, на который он ссылается, и затем передаетmodule.exportsилиexports
Предоставить выходной интерфейс для методов и переменных текущего модуля.
когда у вас есть модуль, который экспортирует только одну вещь, чаще всего используется module.exports:

CommonJS - это больше, чем просто проект по определению общего API и экосистемы для JavaScript. Одна часть CommonJS - это спецификация модуля . Node.js и RingoJS-это серверные среды выполнения JavaScript, и да, оба они реализуют модули, основанные на спецификации модуля CommonJS.
const {
        increase,
        reset
    } = require('./commonJSCounterModule')
    increase()
    reset()
    // или
    const commonJSCounterModule = require('./commonJSCounterModule')
    commonJSCounterModule.increase()
    commonJSCounterModule.reset()

module.exports.default = getName; ///по умолчанию используетсяж
const {default, add } = require('./module') 

class User {
  constructor(name, age, email) {
    this.name = name;
    this.age = age;
    this.email = email;
  }

  getUserStats() {
    return `
      Name: ${this.name}
      Age: ${this.age}
      Email: ${this.email}
    `;
  }
}

module.exports = User;

const User = require('./user');
const jim = new User('Jim', 37, 'jim@example.com');

console.log(jim.getUserStats());

несколько можем экспортирровать
const getName = () => {
  return 'Jim';
};

const getLocation = () => {
  return 'Munich';
};

const dateOfBirth = '12.01.1982';

exports.getName = getName;
exports.getLocation = getLocation;
exports.dob = dateOfBirth;

Назначение свойств для exportsтакже добавляет их module.exports. Это потому, что (по крайней мере, изначально) exportsэто ссылка на module.exports.
у module есть свойство exports, и и спользование exports или module.exports нет разницы так ка ссылаются на один объект module

exports.foo = 'foo';
module.exports = () => { console.log('bar'); };
Это приведет только к экспорту анонимной функции. fooПеременная будет игнорироваться.


function foobar() {
    this.foo = function() {
        console.log('Hello foo');
    }
 
    this.bar = function() {
        console.log('Hello bar');
    }
}
// делаем ее доступной для других модулей
module.exports = foobar;

let foobar = require('./foobar');
let mymodule = new foobar();
 
mymodule.bar(); // 'Hello bar'
//cjs
var myLib = require('myPackage/myLib');
 
function foo(){
    console.log('foo');
    myLib.doSomething();
}
 
//expose module API
exports.foo = foo;

//amd
define(['myPackage/myLib'], function(myLib){
 
    function foo(){
        console.log('foo');
        myLib.doSomething();
    }
 
    //expose module API
    return {
        foo : foo
    };
 
});


//преимущества AMD
//Вы можете создавать и повторно использовать модули, не загрязняя глобальное пространство имен.
//Выполнение кода ассинхронно

 //В основе формата AMD (Asynchronous Module Definition) -
//  это подход к разработке программ, при котором модули и их зависимости могут быть загружены асинхронно.
// Асинхронная загрузка модулей позволяет улучшить скорость загрузки веб-страницы в целом, так как модули загружаются одновременно с остальным контентом сайта.
//Он сводится к описанию модулей функцией define и подключению их с помощью require. 
//<script data-main="/js/app" src="/js/require.js"></script> для запуска Requirejs
Cпецификация AMD (определение асинхронного модуля - определение асинхронного модуля) определяет правила определения модулей. Один файл является модулем, а зависимости модуля и модуля могут загружаться асинхронно.
 Он в основном работает на стороне браузера, который просто подходит для среды модуля асинхронной загрузки браузера, и он не повлияет на работу последующих операторов.
Основные команды модуля работают:define、require return 

AMD (определение асинхронного модуля) - это еще одна спецификация для модулей. RequireJS , вероятно, является самой популярной реализацией AMD. Одно из основных отличий от CommonJS заключается в том, что AMD указывает, что модули загружаются асинхронно - это означает, что модули загружаются параллельно, а не блокируют выполнение, ожидая завершения загрузки.

AMD обычно больше используется в разработке на стороне клиента (в браузере) JavaScript из-за этого, а модули CommonJS обычно используются на стороне сервера. Однако вы можете использовать любую спецификацию модуля в любой среде - например, RequireJS предлагает направления для работы в Node.js, а browserify -это реализация модуля CommonJS, которая может работать в браузере.
 
Новая функция в AMD - это функция define(), которая позволяет модулю объявлять свои зависимости перед загрузкой.

ОТЛИЧИЕ --- require AMD отличается от require CommonJS тем, что в качестве аргументов функции принимает названия модулей и сами модули.

//amd пока зависимости не загрязяться они модуль не определиться, после того как зависимости загрузились они могу с помощью require()юерут() загрузиться асинхронно

define - Это глобальная функция, используемая для определения модуля,define(id?, dependencies?, factory)。require
Команды используются для ввода функций, предоставляемых другими модулями,returnКоманда используется для регулирования внешнего интерфейса модуля,define

require('подгрузка модуля').then(value => {});
//зависимости это модули подгружаются

////////////название модуля зависимости, callback принимает зависимости brequire, exports
define('nameModule',['dep1,dep2'],async function(require,dep1,dep2) {
	const md1 = require('').then().catch(); //асинхронные
		//не вызывется dep1.foo(), если у нас зависимости не загрузились, 
	если загрузились то тогда  запуститься
	const dep1 = await dep1.foo()
})

пока  не объявлен модуль dep2, то модуль который использует зависимости dep2, тоесть dep1, не создаться модуль,
в теле с dep1  и тд и они будут синхронные функции, а если через require().then(res=>res.do()).catch(err) будут асинхронные, а  
define('dep2', function(){
    return {
        do2(){
            console.log('dep2 do2')
        }
    }
})
define('dep1',['dep2'], function(dep2){
  require().then(res=>res.do()).catch(err)
    return {
        do(){
            console.log('dep1 do')
        }
    }
})
define('app', ['dep1','dep2'], function(dep1,dep2, require){
    require('dep2').then(res=>res.do).catch(err=>err);
});
require('app',function(app){
    app.run();
})
 //Функция define() имеет следующую сигнатуру:
define([], function() { //define(["js/module/greeting.js"], function()}),define('name',["js/module/greeting.js"], function()})
    let count = 0;
  return {
    hello: function() {
      console.log('hello' + count++);
    },
    goodbye: function() {
      console.log('goodbye' + count++);
    }
  };
});

require(["js/module/greeting.js"],function(Greeting){ //если названия модуля нет то используется в зависиммости путь к файлу
        Greeting.hello();
        Greeting.goodbye();
}

define('a', {
    add: function(x, y){
         return console.log(x + y);
     }
});

// Use the module (import)
require(['a'], function(a){
    a.add(1, 2);
});

require(['a'], function(a){
    a.add(4, 6);
});

define(
    module_id /*необязательный*/,
    [dependencies] /*необязательный*/,
   // definition function /*функция для создания экземпляра модуля или объекта*/
);


AMD содержит функцию define для определения модуля, которая принимает название модуля, названия зависимостей и фабричную функцию:

  define('amdCounterModule', ['dependencyModule1', 'dependencyModule2'], (dependencyModule1, dependencyModule2) => {
        let count = 0
        const increase = () => ++count
        const reset = () => {
            count = 0
            console.log('Счетчик сброшен.')
        }

        return {
            increase,
            reset
        }
    })

 require(['amdCounterModule'], amdCounterModule => {
        amdCounterModule.increase()
        amdCounterModule.reset()
    })

Динамическая загрузка

Функция define также имеет другое назначение. Она принимает функцию обратного вызова и передает похожую на CommonJS require этой функции. Внутри функции обратного вызова require вызывается для динамической загрузки модуля:

    // используем динамический AMD модуль
 // определяем AMD модуль с CommonJS кодом
    define((require, exports, module) => {
        // CommonJS код
        const dependencyModule1 = require('dependencyModule1')
        const dependencyModule2 = require('dependencyModule2')

        let count = 0
        const increase = () => ++count
        const reset = () => {
            count = 0
            console.log('Счетчик сброшен.')
        }

        exports.increase = increase
        exports.reset = reset
    })

    // используем AMD модуль с CommonJS кодом
    define(require => {
        // CommonJS код
        const counterModule = require('amdCounterModule')
        counterModule.increase()
        counterModule.reset()
    })

define(['jQuery','lodash'], function($, _) {
var name = 'weiqinl',
function foo() {}
return {
name,
foo
}
})
 
// index.js
require(['moduleA'], function(a) {
a.name === 'weiqinl' // true
a.foo () // выполнить функцию foo в модуле A
// do sth...
})
 
// index.html
<script src="js/require.js" data-main="js/index"></script>

//так легче если будет много зависимостей может быть такое,что переменные не там совпадут
 amd
define(function (require) {
    var dependency1 = require('dependency1'),
        dependency2 = require('dependency2');

    return function () {};
});
//преобразует в это

define(['require', 'dependency1', 'dependency2'], function (require) {
    var dependency1 = require('dependency1'),
        dependency2 = require('dependency2');

    return function () {};
});

 //Параметр module_id необязательный, он обычно требуется только при использовании не-AMD инструментов объединения.
  //Когда этот аргумент опущен, модуль называется анонимным. 
 //Параметр dependencies представляет собой массив зависимостей, которые требуются определяемому модулю, 
 //а третий аргумент (definition function) — это функция, которая выполняется для создания экземпляра модуля.
 //Формат модуля — это синтаксис определения и подключения модуля.
 //Также с помощью require() можно динамически импортировать зависимости в модуль:

//UMD
 //Существование двух форматов модулей, несовместимых друг с другом, не способствовало развитию экосистемы JavaScript. 
//Для решения этой проблемы был разработан формат UMD (Universal Module Definition). 
//Этот формат позволяет использовать один и тот же модуль и с инструментами AMD, и в средах CommonJS.
//Суть подхода UMD заключается в проверке поддержки того или иного формата и объявлении модуля соответствующим образом.
Режим UMD (универсальное определение модуля - универсальное определение модуля), этот режим в основном используется для решенияCommonJSРежим иAMD

Если define обнаружена, фабричная функция вызывается через нее.
Если define не обнаружена, фабричная функция вызывается напрямую. В этот момент аргумент root — это объект Window браузера. 
Он получает зависимые модули из глобальных переменных (свойств объекта Window). Когда factory возвращает модуль, он также становится глобальной переменной (свойством объекта Window).


(function (root, factory) {
if (typeof define === 'function' && define.amd) {
 // Режим AMD. Регистрация в качестве анонимной функции
define(['b'], factory);
} else if (typeof module === 'object' && module.exports) {
 // CommonJS-подобная среда, такая как Node
module.exports = factory(require('b'));
} else {
 // Обозреватель глобальных переменных (root is window)
root.returnExports = factory(root.b);
}
}(typeof self !== 'undefined' ? self : this, function (b) {
 // используем b каким-то образом
 
 // Возвращаем значение для определения модуля экспорта. (Т.е. вы можете вернуть объект или функцию)
return {};
// (function (root, factory) {
// if (typeof define === "function" && define.amd) {
// define(["jquery", "underscore"], factory);
// } else if (typeof exports === "object") {
// module.exports = factory(require("jquery"), require("underscore"));
// } else {
// root.Requester = factory(root.$, root._);
// }
// }(this, function ($, _) {
// // this is where I defined my module implementation

// var Requester = { // ... };

// return Requester;
// }));

 //модуль es6 
//это export и import
 //Экспортированное по умолчанию свойство считается «главным» в этом модуле.
//Суть дефолтного экспорта — при импорте можно назвать его, как угодно.
//все ипортить через import * as sub from './sub.js';
//если некоторые то import {add, minus} from './sub.js';
//as -чтобы менять название
//Реэкспорт то есть обьявляешь в файле а потом и еще в другом export
//export * from './another';
//Однако важно помнить, что если вы объявите в своём модуле экспорт с таким же именем,
// как у реэкспортного, то он затрёт реэскпортное.

es6 динамическая подгрузка Данная функция возвращает промис, поэтому использовать модуль можно с помощью then:
import('./esCounterModule.js').then(dynamicESCounterModule => {
        dynamicESCounterModule.increase()
        dynamicESCounterModule.reset()
    })

export default surfaceArea = (r) => {
  return 4 * pi * square(r);
};
import surfaceArea from './math.js';
const surfaceOfMars = surfaceArea(3390);

export function sayHi(user) {
  alert(`Hello, ${user}!`);
}
import {sayHi} from './say.js';// обязательно писать начинать с './say.js' а то не правельный импорт 'say'
alert(sayHi); // function...
sayHi('John'); // Hello, John!

<script type="module">
  import {sayHi} from './say.js';
  document.body.innerHTML = sayHi('John');

  let name = 'Artem';
</script> 
<script type="module"> // не будет видно так как область видимости только
 alert(name) //небудет видно так как для скриптов своя область видимость но можно сделать глобальный window.name='Artem'
</script> 

<script nomodule>alert('nomodule для старых браузеров')</script> // но новые браузеры понимают
<script type="module">
  alert(this); // undefined у модуля нет this
</script>

// если мы в изменим объект, то он будет меняться и в последующих файлах
export let admin = { };

export function sayHi() {
  alert(`Ready to serve, ${admin.name}!`);
}
//файл 1
import {admin} from './admin.js';
admin.name = "Pete";

//файл 2
import {admin, sayHi} from './admin.js';
alert(admin.name); // Pete
sayHi();// Ready to serve, Pete!
