СУБД – это система управления базами данных 

create database название базы данных;                     //СОЗДАНИЕ БАЗЫ ДАННЫХ

show database; - отобразить список доступных баз данных   // ВЫВОД ВСЕХ БАЗ ДАННЫХ

DROP DATABASE [IF EXISTS] название базы данных           //УДАЛЕНИЕ БАЗЫ ДАННЫХ [IF EXISTS](проверяет есть ли база данных ) 

RENAME TABLE старое_название TO новое_название;          // ПЕРЕИМЕНОВАНИЕ  ТАБЛИЦЫ

TRUNCATE TABLE название таблицы;                         // ДЛЯ ПОЛНОЙ ОЧИСТКИ ТАБЛИЦЫ

DROP TABLE название таблицы;                              //УДАЛЕНИЕ ТАБЛИЦЫ

use название базы данных -                              //ДЛЯ ТОГО ЧТОБЫ ИСПОЛЬЗОВАТЬ БАЗУ ДАННЫХ

                                                        //ОТЛИЧИЕ CHAR от VARCHAR
char(10) представляет стоку фиксированной длины. то есть если указано 10, 
а ты вписал 4 то прибавится еще 6 пробелов
varchar представляет стоку переменной длины. а тут так и останеться до 10

float
int
Double
Date
Time
DATETime
Year

numeric(5,2) 123.00 5-  точность 5 а маштаб 3
demical(10,5)  12345.12 - 12345.12000-десятичное число 5 - это максимальное хранимое число после точки, 10 тоность, общее количество

CREATE TABLE IF NOT EXISTS `Users`(                                       //СОЗДАНИЕ ТАБЛИЦЫ
   `id` INT NOT NULL AUTO_INCREMENT,
   `name` VARCHAR(100) NOT NULL, CHECK(name !='')                     //CHECK - условие которуму должно соответсвовать
   `age` INT(11) // DEFAULT 18 указать значени по умолчанию
   submission_date DATE,
 //CHECK((age >0 AND age<100) AND (name !='') ) // на уровне таблицы можно использовать
   PRIMARY KEY ( id )
);

CREATE TABLE IF NOT EXISTS `Friends`(
   `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
   `name` VARCHAR(100) NOT NULL,                   //UNIQUE уникальное поле , тоесть не добавит с таким же именем
   `age` INT(11)
    user_id INT
   submission_date DATE,
   
 FOREIGN KEY (user_id)  REFERENCES Users (id) для связывание таблиц по id // ON DELETE CASCADE ON UPDATE CASCADE

);


CASCADE: автоматически удаляет или изменяет строки из зависимой таблицы при удалении или изменении связанных 
строк в главной таблице.

                           // ДОБАВЛЕНИЕ И УДАЛЕНИЕ СТОЛБЦА ТАБЛИЦЫ
добавление - ALTER TABLE Customers ADD Address VARCHAR(50) NULL;
удаление - ALTER TABLE Customers DROP COLUMN Address;


                          //ИЗМЕНЕНИЕ ТИПА СТОЛБЦА ТАБЛИЦЫ
ALTER TABLE Customers MODIFY COLUMN FirstName CHAR(100) NULL;

                        //ИЗМЕНЕНИЕ ЗНАЧЕНИЯ ПО УМОЛЧАНИЮ
ALTER TABLE Customers ALTER COLUMN Age SET DEFAULT 22;

                          // ДОБАВЛЕНИЕ И УДАЛЕНИЕ ПЕРВИЧНОГО КЛЮЧА
добавление - ALTER TABLE Products ADD PRIMARY KEY (Id);
удаление  - ALTER TABLE Products DROP PRIMARY KEY;


                        //ДОБАВЛЕНИЕ И УДАЛЕНИЕ ВНЕШНЕГО КЛЮЧА

добавление - 
ALTER TABLE Orders 
ADD CONSTRAINT orders_customers_fk 
FOREIGN KEY(CustomerId) REFERENCES Customers(Id);

удаление - ALTER TABLE Orders DROP FOREIGN KEY orders_customers_fk;

                              //ДОБАВЛЕНИЕ ДАННЫХ В ТАБЛИЦУ
INSERT INTO названее таблицы (id, state, population) VALUES (NULL, ‘Alabama’, ‘4822023’);

множественный update
INSERT INTO t (id,name)
    VALUES
    (1, '2'),
    (2, '2'),
    (3, '2'),
    (4, '6')
ON DUPLICATE KEY UPDATE 
    name = VALUES(name);

//ON DUPLICATE KEY UPDATE в свою очередь просто обновляет запись без удаления, что конечно происходит быстрее. Так же нужно помнить что, если у вас есть какие-то поля (не указанные в запросе) со значениями отличными от DEFAULT, то UPDATE их не затронет

множественный insert 

 insert into goods  
    (id,good,price)  
    values
        (1, 'Ноутбук', 30000),
        (2, 'Телефон', 5000),
        (3, 'Смартфон', 10000),
        (4, 'Планшет', 15000)

//множественное удаление 
delete from t where id >= 1 and id<= 3;
delete from t where id between 1 and 3; //удалит 1 2 3
                              //ОБНОВЛЕНИЕ
UPDATE Products SET Price = Price + 3000; увеличим у всех строк price значения
UPDATE Products SET Price = Price + 3000 Where = id =1; для конкретного занчения

                            //УДАЛЕНИЕ СТРОК ИЗ СТОЛБЦА
DELETE FROM Products WHERE Manufacturer='Huawei'; по условию 

                            //ВЫБОРКА НА ВЫВОД ДАННЫХ
Select * from название таблицы или же определенные Select name from название таблицы

Select u.name from users as u;

                        // С УМНОЖЕНИЕ ПОЛЯ НА 15
Select name, price *  15 as totalPrice from users 

                        // ВЫВЕДЕТ У КОГО ИМЯ АРТЕМ
Select name from users where name = 'Artem' 

                        //ВЫВЕДЕТ У КОГО ИМЯ АРТЕМ ИЛИ ЦЕНА МЕНЬШЕ 500
Select name,price, from users where name ='Artem' or price<500 

                        //ВЫВЕДЕТ У КОГО ИМЯ АРТЕМ И ЦЕНА МЕНЬШЕ 500
Select name,price, from users where name ='Artem' and price<500 

                        //ВЫВЕДЕТ У КОГО ИМЯ НЕ АРТЕМ
Select name,price, from users where not name ='Artem' 

                        //ВЫБОРКА УНИКАЛЬНЫХ ЗНАЧЕНИЙ ПО ПОЛЯМ ИЛИ ПОЛЮ
SELECT DISTINCT Manufacturer,ProductCount FROM Products; 

                        //ВЫБОРКА ЗАПИСЕЙ КОТОРАЯ ВХОДИТ В ПЕРЕЧИСЛЕНИЕ IN()
SELECT * FROM Products WHERE Manufacturer IN ('Samsung', 'HTC', 'Huawei'); 

                        //ВЫБОРКА ЗАПИСЕЙ КОТОРЫЕ НЕ ВХОДИТ В ПЕРЕЧИСЛЕНИЕ IN()
SELECT * FROM Products WHERE Manufacturer IN ('Samsung', 'HTC', 'Huawei');

                        //ВЫБОРКА ЗАПИСЕЙ КОТОРАЯ ВХОДИТ В ДИАПОЗОН
SELECT * FROM Products WHERE Price NOT BETWEEN 20000 AND 50000; 

                        //ВЫБОРКА ЗАПИСЕЙ КОТОРЫЕ НЕ ВХОДИТ В ДИАПОЗОН
SELECT * FROM Products WHERE Price NOT BETWEEN 20000 AND 50000; 

                          //ВЫБОРКА ЗАПИСЕЙ С (LIKE) КОТОРЫЕ КОТОРЫЕ ИДУТ ПОСЛЕ %. тоесть после 'Iphone'
SELECT * FROM Products WHERE ProductName LIKE 'iPhone%'

                        //ВЫБОРКА ЗАПИСЕЙ С (LIKE) КОТОРЫЕ КОТОРЫЕ ИДУТ ПОСЛЕ _. ОДИНОЧНЫЙ СИМВОЛ Iphone7 или 8
SELECT * FROM Products WHERE ProductName LIKE 'iPhone_';

                        //ВЫБОРКА ДАННЫХ (IS NULL)  ПОЗВОЛЯЕТ ВЫБРАТЬ ВСЕ СТРОКИ КОТОРЫЕ ИМЕЮ NULL
SELECT * FROM Products WHERE ProductCount IS NULL;

                        //ВЫБОРКА ДАННЫХ (IS NOT NULL)  ПОЗВОЛЯЕТ ВЫБРАТЬ ВСЕ СТРОКИ КОТОРЫЕ НЕ ИМЕЮ NULL
SELECT * FROM Products WHERE ProductCount IS NOT NULL; 

                    //(ORDER BY) СОРТИРУЕТ ПО ОДНОМУ ИЛИ НЕСКОЛЬКИМ СТОЛБЦАМ.
SELECT * FROM Products ORDER BY Price;                //ORDER BY Price  СОРТИРУЕТ ПО ВОЗРАСТАНИЮ (ПО УМОЛЧАНИЮ)
SELECT * FROM Products  ORDER BY Price ASC;           //ORDER BY Price ASC СОРТИРУЕТ ПО ВОЗРАСТАНИЮ
SELECT * FROM Products ORDER BY Price DESC;           //ORDER BY Price DESC СОРТИРУЕТ ПО УБЫВАНИЮ
SELECT ProductName, Price, Manufacturer FROM Products ORDER BY Manufacturer ASC, ProductName DESC;

                  //LIMIT 3 ВЫВОДИТ ПЕРВЫЕ ТРИ СТРОКИ, LIMIT 2,3 ПЕРВЫЕ ДВЕ ПРОПУСКАЕТ И ПОТОМ 3 БЕРУТЬСЯ
SELECT * FROM Products  LIMIT 3;    // первые три строки
SELECT * FROM Products  LIMIT 2, 3; // две первые пропускаются, и потом 3 беруться

AVG - СРЕДНЕЕ ЗНАЧЕНИЕ ВЫВОДИТ,
MIN - МИНИМАЛЬНОЕ ЗНАЧЕНИЕ ВЫВОДИТ, 
MAX - МАКСИМАЛЬНОЕ ЗНАЧЕНИЕ ВЫВОДИТ,
COUNT КОЛИЧЕСТВО, 
SUM - СУММУ

SELECT AVG(Price) AS Average_Price FROM Products

                    //ГРУППИРОВКА 
                    //ВЫВЕДЕТ НАЗВАНИЯ УНИКАЛЬНЫЕ ПО ПОЛЮ И КОЛИЧЕСТВО ЗАПИСЕЙ С ТАКИМ ПОЛЕМ
SELECT Manufacturer, COUNT(*) AS ModelsCount FROM Products GROUP BY Manufacturer 

                    //ФИЛЬТРАЦИЯ ГРУПП HAVING
//  HAVING - HAVING позволяет выполнить фильтрацию групп, то есть определяет, 
// какие группы будут включены в выходной результат.

//найдем все группы товаров по производителям, для которых определено более 1 модели
SELECT Manufacturer, COUNT(*) AS ModelsCount FROM Products GROUP BY Manufacturer HAVING COUNT(*) > 1

                                              //ПОДЗАПРОС ВЫВОД по id подзапроса выведет
SELECT *, (SELECT ProductName FROM Products WHERE Id=Orders.ProductId) AS Product FROM Orders 

                                          //НЕ ВКЛЮЧАЮЩИЕ ИЗ ПОДЗАПРОСА
SELECT * FROM Products WHERE Id NOT IN (SELECT ProductId FROM Orders) 

                                              //ДЛЯ ИЗМЕНЕНИЯ ПОДЗАПРОС
UPDATE Orders SET Price = (SELECT Price FROM Products WHERE Id=Orders.ProductId) + 3000 WHERE Id=1;

UPDATE Orders SET ProductCount = ProductCount + 2 
WHERE ProductId IN (SELECT Id FROM Products WHERE Manufacturer='Apple');

                              //УДАЛЕНИЕ ПОД ЗАПРОС
DELETE FROM Orders
WHERE ProductId=(SELECT Id FROM Products WHERE ProductName='Galaxy S8');



Inner join - позволяет извлекать строки, которые обязательно присутствуют во всех объединяемых таблица
но для того чтобы выбрать которые сопоставимы друг гругу 

выведит таблицу только те строки у которых id совпали 
SELECT * FROM nomenclature INNER JOIN description using(id);
или
SELECT * FROM nomenclature inner join description on nomenclature.id = description.id;

выведет таблицу где будут все строки левой таблицы а правой таблицы у которых id не совпало то null
Left join выведет все строки левой таблицы а правой если по id нет совпадения то в поле выведет null
SELECT * FROM nomenclature LEFT JOIN description USING(id);

или SELECT * FROM nomenclature left join description on nomenclature.id = description.id;

выведет таблицу где будут все строки правой таблицы а левой таблицы у которых id не совпало то null
Right join выведет все строки правой таблицы а в левой если по id нет совпадения то в поле выведет null
SELECT * FROM nomenclature left join description on nomenclature.id = description.id; 
 

Запрос с оператором INNER JOIN предназначен для соединения таблиц и вывода результирующей таблицы, в которой данные полностью пересекаются по условию, указанному после ON.

Запрос с оператором LEFT OUTER JOIN предназначен для соединения таблиц и вывода результирующей таблицы, в которой данные полностью пересекаются по условию, указанному после ON, 
и дополняются записями из первой по порядку (левой) таблицы, даже если они не соответствуют условию. У записей левой таблицы, которые не соответствуют условию, значение столбца из правой таблицы будет NULL (неопределённым).

Запрос с оператором RIGHT OUTER JOIN предназначен для соединения таблиц и вывода результирующей таблицы, в которой данные полностью пересекаются по условию, указанному после ON, 
и дополняются записями из второй по порядку (правой) таблицы, даже если они не соответствуют условию. У записей правой таблицы, которые не соответствуют условию, значение столбца из левой таблицы будет NULL (неопределённым).

Запрос с оператором FULL OUTER JOIN предназначен для соединения таблиц и вывода результирующей таблицы, в которой данные полностью пересекаются по условию, указанному после ON, и дополняются записями из первой (левой) 
и второй (правой) таблиц, даже если они не соответствуют условию. У записей, которые не соответствуют условию, значение столбцов из другой таблицы будет NULL (неопределённым).

выведет и правую и левую калонку а где не совпадает Null поставит

(SELECT p.id, p.name `Имя сотрудника`, ps.id `pos.id`, ps.name `Должность`
FROM `persons` p
LEFT OUTER JOIN `positions` ps ON ps.id = p.post_id)
UNION
(SELECT p.id, p.name `Имя сотрудника`, ps.id `pos.id`, ps.name `Должность`
FROM `persons` p
RIGHT OUTER JOIN `positions` ps ON ps.id = p.post_id)

//левое подмножество только выводит, выведит те строки у которых в правой таблице не совпали и у поля стоит NULL
SELECT p.id, p.name `Имя сотрудника`, ps.id `pos.id`, ps.name `Должность`
FROM `persons` p
LEFT OUTER JOIN `positions` ps ON ps.id = p.post_id
WHERE ps.id is NULL

//правое подмножество только выводит, выведит те строки у которых в левой таблице не совпали и у поля стоит NULL
SELECT p.id, p.name `Имя сотрудника`, ps.id `pos.id`, ps.name `Должность`
FROM `persons` p
RIGHT OUTER JOIN `positions` ps ON ps.id = p.post_id
WHERE p.id is NULL

// все кроме пересечения выведет
(SELECT p.id, p.name `Имя сотрудника`, ps.id `pos.id`, ps.name `Должность`
FROM `persons` p
LEFT OUTER JOIN `positions` ps ON ps.id = p.post_id
WHERE ps.id is NULL)
UNION ALL
(SELECT p.id, p.name `Имя сотрудника`, ps.id `pos.id`, ps.name `Должность`
FROM `persons` p
RIGHT OUTER JOIN `positions` ps ON ps.id = p.post_id
WHERE p.id is NULL)

                                                        //UNION/
// Команда UNION объединяет данные из нескольких таблиц в одну при выборке.
// При объединении количество столбцов во всех таблицах должно совпадать, иначе будет ошибка
// Имена столбцов будут такие же, как в основной таблице, в которую добавляются данные из других таблиц.
// Внимание: если не используется ключевое слово ALL для UNION, все возвращенные строки будут уникальными, так как по умолчанию подразумевается distinct, который удаляет неуникальные значения.
// Чтобы отменить такое поведение - нужно указать ключевое слово ALL, вот так: UNION ALL.
//это запрет пустых значений (NULL) в любом столбце объединения,
 SELECT id, name, 0 as country_id FROM countries // 0 as country_id можно добовлять поле если различаются
	UNION SELECT id, name, country_id FROM cities   // если не добавить то будет ошибка, потому что количество должно 
                                                  //быть одинаковым те у котрого нет будет заменять на 0 так как 0 as county_id
// если у нас id name поля одинаковые в строке то не дублируется а если хоть одно поле разное то добавиться

SELECT * FROM имя_таблицы1 WHERE условие
	UNION SELECT * FROM имя_таблицы2 WHERE условие

SELECT * FROM имя_таблицы 1 WHERE условие
	UNION SELECT * FROM имя_таблицы2 WHERE условие
	UNION SELECT * FROM имя_таблицы3 WHERE условие
	UNION SELECT * FROM имя_таблицы4 WHERE условие  

// Нормализация
// Нормализация – это метод проектирования базы данных, который позволяет привести базу данных к минимальной избыточности.
// Избыточность устраняется, как правило, за счёт декомпозиции отношений (таблиц), т.е. разбиения одной таблицы на несколько

1 нф = это чтобы не было кортежей в одном атрибуте
2нф = это 1 нф и чтобы были первичные ключи, сделая декомпозицию разбить надо таблицу. посмотреть что зависит и что не зависсит не зависимые перенести в другую таблицу(все неключевые атрибуты вынести в другую таблицу)
3нф = это 2 нф, поле не ключевое зависит от другого не ключего поля(выносить все не ключевые поля, содержимое которых может относиться к нескольким записям таблицы в отдельные таблицы)

1 нормализация заключается в том чтобы в одной ячейке не было ни массивов, ни списков
В таблице не должно быть дублирующих строк
В каждой ячейке таблицы хранится атомарное значение (одно не составное значение)
В столбце хранятся данные одного типа
Отсутствуют массивы и списки в любом виде
//неправильно
  Брэнд           Тип       Категория
  Adidas,Nike     Футболка     мужское
  Puma            Шорты      женское
  

//правилььно
  Брэнд           Тип       Категория
  Adidas        Футболка     мужское
  Nike          Футболка     мужское
  Puma            Шорты      женское

2 нормальная форма
Таблица должна находиться в первой нормальной форме
Таблица должен  иметь первичный ключ
Все неключевые столбцы таблицы должны зависеть от первичного ключа 
//неправильно , потому скидка зависит от категории мужского или женского , в данном случаем мы сможем изменить мужскуюю катег
articul  Брэнд           Тип       Категория    Скидка
dsags    Adidas        Футболка     мужское       10%
dat      Nike          Футболка     мужское       10%    //20% это не правильно должны менять полностью в категории мужской
djdr     Puma            Шорты      женское       15%
 
 //правильно
таблица товаров
articul  Брэнд           Тип       Id_cat
dsags    Adidas        Футболка     1
dat      Nike          Футболка     1
djdr     Puma            Шорты      2

категории товаров
id    Категория    Скидка
1      мужское       10%
2       женское      15%

3 она удовлетворяет всем предыдущим нормальным формам и любой ее  не ключевой атрибут функционально зависит от первичного ключа

тоже самое сделаем ка во торой нормальной форме вынесем в таблицы

//неправильно
таблица товаров
articul  Брэнд           Тип       Id_cat   Цена
dsags    Adidas        Футболка     1       100
dat      Nike          Футболка     1       200
djdr     Puma            Шорты      2       150

категории товаров
id    Категория    Скидка
1      мужское       10%
2       женское      15%

//правильно
таблица товаров
articul  ID_BRAND   ID_Type   Id_cat
dsags    1            1         1
dat      2            1         1
djdr     3            2         2

категории товаров 
id    Категория    Скидка
1      мужское       10%
2       женское      15%

 Брэнд          Id
  Adidas        1
   Nike         2
   Puma         3

   Тип          Id
  Футболка      1
  Шорты         2

1НФ   - отношение содержит только неделимые значения в каждом поле (столбце) строки.

2НФ - таблица имеет первичный ключ и отсутствуют зависимости неключевых полей от части составного ключа.

3НФ – из таблицы вынесены все не ключевые поля содержимое которых может относиться к нескольким записям таблицы в отдельные таблицы .

Значение NULL не является значением в полном смысле слова: по определению оно означает отсутствие значения и не принадлежит ни одному типу данных. 
Поэтому NULL не равно ни логическому значению FALSE, ни пустой строке, ни нулю.

Уникальный столбец (или уникальная группа столбцов), используемый чтобы идентифицировать каждую строку и храненить все строки отдельно, называются — первичными ключами таблицы.

