// Объявление и использование переменных - 1

// Три типа переменных var, let, const

// не правельные задданные емена переменных
// let 1a;
// let my-name;

//var область видимости функциональная
function foo() {
    var a = 2;
}

console.log(a); // a is not defined

if (true) {
    var a = 3;
}

console.log(a); // 3

function bar() {
    for (var i = 0; i < 3; i++) {
        console.log(i); // 0 1 2
    }
    console.log(i); //3
}

bar();

var num1 = 1;
var num1 = 2;
console.log(num1); // 2 так как переопределит

console.log(num3); //undefined
var num3 = 4;
// почему так происходит 
var num3; // поднимается в свою область переменная
console.log(num3); // greeter is undefined
var num3 = 4;

// let и  const блочная область видимоси
// отличие вмежду let и const, в том что const иммутабельный, но свойства объекта менять можно

console.log(num5, num6); // is not defined, перемещается вверх, но не инициализируется 
let num5 = 2;
const num6 = 3;

let num7 = 1;
let num7 = 2;
console.log(num7); //has already been declared также и с const

let num8 = 3;
num8 = 4;
console.log(num8); // 4

const num9 = 3;
num9 = 4;
console.log(num9); //assignment constant variable

let num10 = 1;
if (true) {
    console.log(num10); // cannot access num10 before  initilization
    let num10 = 2;
    console.log(num10); //2
}
console.log(num10); // 1

function foobar() {
    let num11 = 1;
    if (true) {
        console.log(num11); // cannot access num11 before  initilization
        let num11 = 2;
        console.log(num11); // 2
    }
}
foobar();

// Объявление и использование функций - 2

//Function Declaration - позволяет вызвать перед объявлением
hello(); //Hello
function hello() { console.log('Hello') }

//Function Expression - использовать можно только после объявления
hello(); // cannot access hello before  initilization
let hello = function() { console.log('Hello') }
hello(); //Hello

//стрелочная функция нет this и псевдомассива arguments
hello(); // cannot access hello before  initilization
let hello = () => { console.log('Hello') }
hello(); //Hello

//через new Function - из строки
// особенность записывается на на внешнее лексическое окружение, а на глобальное
let sum = new Function('a', 'b', 'return a + b');
console.log(sum(1, 2)); //3

function getFunc() {
    let value = "test";

    let func = new Function('alert(value)');

    return func;
}

getFunc()(); //  value не определено

//immediately-invoked function expressions функция немедленного вызова
(function() {
    var foo = '';
}());

let anonym = function() {
    console.log("это анонимная функция");
};
// в данном случае name() мы может вызвать только внутри функции можем тоесть сделать каккойто повторный результат
let anonym = function name() {
    console.log("это анонимно именновая функция");
};


// Область видимости. Что такое «поднятие» (hoisting) - 3
// область - область где доступны переменная. Всего три области: глобальная, функциональная, блочная
//hoisting - суть способность переменных и function declaration подниматься в области в котором они были обьявлены

var num3; // поднимается в свою область переменная
console.log(num3); // greeter is undefined
var num3 = 4;

//блочная область
function foobar() {
    let num11 = 1;
    if (true) {
        console.log(num11); // cannot access num11 before  initilization
        let num11 = 2;
        console.log(num11); // 2
    }
}
foobar();

//функциональная область
function foo() {
    var a = 2;
}

console.log(a); // a is not defined

//глобальная область
var a = 3;
