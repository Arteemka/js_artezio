let uniqueItemId = 0;

// function generateID() {
//     return function(prefix) {
//         return (prefix || '') + ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
//             (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16))
//     };
// }

// console.log(generateID()('uuid-'));

// let unique = (function() {
//     return function(prefix) {
//         return (prefix || '') + ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
//             (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16))
//     }
// })();

// console.log(unique("uuid-"));

function uniqueId() {
    return uniqueItemId++;
}

class StorageItem {
    id = -1;
    constructor(name) {
        this.name = name;
    }
}

class ArrayStorage {
    _items = [];

    getlist() {
        return this._items.sort((a, b) => a.id - b.id);;
    }

    addItem(item) {
        item.id = uniqueId();
        this._items.push(item);
        return item;
    }

    hasItemWithId(id) {
        return this._items.some(item => item.id === id)
    }

    deleteItem(id) {
        if (!this.hasItemWithId(id)) return false;

        this._items = this._items.filter(item => item.id !== id);

        return true;
    }

    getItem(id) {
        return this._items.find(item => item.id === id);
    }

    updateItem(id, newObject) {
        const item = this.getItem(id);

        Object.keys(newObject || {}).forEach(key => {
            if (item[key] !== undefined) item[key] = newObject[key];
        });


        return item;
    }
}


let store = new ArrayStorage;

store.addItem(new StorageItem("Artemiy"));
store.addItem(new StorageItem("Aleksiy"));
store.addItem(new StorageItem("Dima"));
store.addItem(new StorageItem("Aleksiy"));
store.addItem(new StorageItem("Artemiy"));

console.log('ArrayStorage: getList', store.getlist());
console.log('ArrayStorage: getItem ', store.getItem(2));
console.log('ArrayStorage: updateItem', store.updateItem(2, { name: 'Dmitry', secondName: 'Krasov' }));
console.log('ArrayStorage: deleteItem', store.deleteItem(3));
console.log('ArrayStorage: getList', store.getlist());


class MapStorage {
    _items = new Map();

    getlist() {
        return Array.from(this._items.values()).sort((a, b) => a.id - b.id);
    }

    addItem(item) {
        item.id = uniqueId();
        this._items.set(item.id, item);
    }

    hasItemWithId(id) {
        return Array.from(this._items.values()).some(item => item.id === id)
    }

    deleteItem(id) {
        return this._items.delete(id);
    }

    getItem(id) {
        return this._items.get(id);
    }

    updateItem(id, newObject) {
        const item = this.getItem(id);

        Object.keys(newObject).forEach(key => {
            if (item[key] !== undefined) item[key] = newObject[key];
        });

        return item;
    }
}



let store1 = new MapStorage;

store1.addItem(new StorageItem("Artemiy"));
store1.addItem(new StorageItem("Aleksiy"));
store1.addItem(new StorageItem("Dima"));

console.log('MapStorage: getList', store1.getlist());
console.log('MapStorage: getItem', store1.getItem(5));
console.log('MapStorage: updateItem', store1.updateItem(7, { name: 'Dmitry' }));
console.log('MapStorage: getList', store1.getlist());
console.log('MapStorage: deleteItem', store1.deleteItem(5));
console.log('MapStorage: getList', store1.getlist());