// Array - структура данных, в котором элементы расположены друг за другом и со своими методами
  // способы создания массивов

     let arr = new Array();
     let arr = [];

let arr= [1,2,3];
delete arr[1];
arr // [1, undefined, 3]
     Array(3) //создать 3 пустых элемента, если попробвать получить будет undefined, длина 3
   let arr = [3] // один элемент будет
  // Доступ по индексу к элементам
    
     let arr = ['1', 2, '3'];
     console.log(arr[0]);              // '1'
     console.log(arr[1]);              // 2
     console.log(arr[arr.length - 1]); // '3'
  
  // Методы массивов
    
    // forEach - позволяет перебрать массив, ничего не возвращая? и запуская функцию для каждого элемента массива
       let arr = ['1', 2, '3'];
       arr.forEach(function(item, index, array) {
             console.log(item, index); // 1,0 , 2,1 , 3,2 
       });

    // push - позволяет добовлять элементв конец
       arr.push(4);
       console.log(arr) // ['1', 2, '3', 4];
   
   // pop - позволяет удалить последний элемент
       arr.pop();
       console.log(arr) // ['1', 2, '3'];

   // unshift - позволяет добавить элемент в начала массива 
      arr.unshift(4);
      console.log(arr) // [4,'1', 2, '3'];

   // shift -  позволяет удалить элемент из начала массива
      arr.shift();
      console.log(arr) // ['1', 2, '3'];

   // поиск в массиве
     // arr.indexOf(item, from) // возвращает индекс элемента, from с какого индекса начинать

        console.log(arr.indexOf(2)) // 1
     // arr.lastIndexOf(item, from) // все как у indexOf только поиск начинает с конца в начало

       console.log(arr.lastIndexOf('1')) // 0
     // includes - ищет элементв массиве возвращая true или false 
      
       console.log(arr.indexOf(2)) // true
     
     // find - возвращает первый найденный элемент, который удовлетворяет условию переданному в callback функции
     // иначе возвращает undefined
     let arr = ['1', 2, '3', 4]
     console.log(arr.find(item => typeof item === 'number' ))) // 2
    // findIndex делает тоже самое только возвращает индекс найденного элемента
     
      console.log(arr.findIndex(item => typeof item === 'number' ))) // 1
    // splice позволяет добавлять, удалять, изменять элементы массива
      // если мы удалим таким способом delete arr[1] - то удалим только значение, и будет пустое значение , тоесть

        let arr = ['1', 2, '3', 4] , delete arr[2] // arr = ['1', 2, empty, 4]
      // удаление через splice

         let arr = ['1', 2, '3', 4] 
         arr.splice(1, 1); // начиная с позиции 1, удалить 1 элемент arr = ['1', '3', 4]
      // заменяет значения

         let arr = ['1', 2, '3', 4] 
         arr.splice(0, 1, 3, 4); // arr = ['1', 3, 4, 4]
     
      // добовление элементов
        
         let arr = ['1', 2, '3', 4] 
         arr.splice(3, 0, 5, 6); // arr = ['1', 2, '3', 4,5,6] 
      // можно с отрицательными, удаление

         let  arr = ['1', 2, '3', 4,5,6];
         arr.splice(-1,1) // arr = ['1', 2, '3',4,5];  -1 - означает перед последним элементом
    // копировать массив
 
        let  arr = ['1', 2, '3',6];
        let arr2 = arr.splice(); 
        console.log(arr2) // arr2 = ['1', 2, '3',6];

    // slice - Он возвращает новый массив, в который копирует элементы, начиная с индекса start и до end

        let  arr = ['1', 2, '3',6];
        console.log(arr.slice(1)); // arr = [ 2, '3',6];
        console.log(arr.slice(1,3)); // arr = [ 2, '3'];
        console.log( arr.slice(-2) );  //  arr = [ '3', 6];

    // копирование  массива

        let  arr = ['1', 2, '3',6];
        let arr2 = arr.splice();
        console.log(arr2) // arr2 = ['1', 2, '3',6];
    // concat создаёт новый массив, в который копирует данные из других массивов 
   
       let  arr = ['1', 2, '3',6];
       console.log(arr.concat([7,8])) // arr = ['1', 2, '3',6,7,8];

    // с объектом нужно использовать Symbol.isConcatSpreadable,иначе будет [Object object]
    // он обрабатывается concat как массив: вместо него добавляются его числовые свойства
       
       let arr = [1, 2];
      let obj = {
          0: 3,
          1: 4,
         [Symbol.isConcatSpreadable]: true,
         length: 2
      }
     console.log(arr.concat(obj)) // // 1,2,3,4
  
     // sort сортирует на местеэлемент возвращает отсортированный массив
       // в данном примере это связанно с тем что числа преобразует в строку и сравнивает как строку
       // также  числа находятся перед буквами в верхнем регистре, верхний регистр перед нижним регистром
  // Если в sort внутри не пишется функция, массив сортируется в соответствии со значениями кодовых точек каждого символа Unicode, полученных путём преобразования каждого элемента в строку.При числовой сортировке, 9 идёт перед 80, но поскольку числа преобразуются в строки, то "80" идёт перед "9" в соответствии с порядком в Unicode.
  //Если compareFunction(a, b) меньше 0, сортировка поставит a по меньшему индексу, чем b, то есть, a идёт первым.
//Если compareFunction(a, b) вернёт 0, сортировка оставит a и b неизменными по отношению друг к другу, но отсортирует их по отношению ко всем другим элементам. Обратите внимание: стандарт ECMAscript не гарантирует данное поведение, и ему следуют не все браузеры (например, версии Mozilla по крайней мере, до 2003 года).
//Если compareFunction(a, b) больше 0, сортировка поставит b по меньшему индексу, чем a.
        let arr1 = ['слово', 'Слово', '1 Слово', '2 Слова'];
         arr.sort() // ['1 Слово', '2 Слова', 'Слово', 'слово']
        let  arr = [1, 30, 10,2];
        arr.sort() // возвратит arr = [1, 10, 2,30];
        function compareNumbers(a, b) {
  return a - b;
} //возрастание , убывание на оборот
var items = [
  { name: 'Edward', value: 21 },
  { name: 'Sharpe', value: 37 },
  { name: 'And', value: 45 },
  { name: 'The', value: -12 },
  { name: 'Magnetic' },
  { name: 'Zeros', value: 37 }
];
items.sort(function (a, b) {
  if (a.name > b.name) {
    return 1;
  }
  if (a.name < b.name) {
    return -1;
  }
  // a должно быть равным b
  return 0;
}); // по возрастанию будет
     // some() проверяет, удовлетворяет ли какой-либо элемент массива условию

        const array = [1, 2, 3, 4, 5];


        

console.log(array.some(item => item % 2 === 0)); // true

    // reverse - на месте обращает порядок следования элементов массива
       const arr = ['2', '1', '3'];

       console.log( arr); // arr = ['3','1', '2'];

   // map  возвращает преобразованный массив, проходя по каждому элементу
     let arr = [1, 4, 9];
     let doubles = arr.map(num => num * 2); 
     console.log(doubles) // arr = [1,8,18]

   // keys() возвращает новый итератор массива Array Iterator, содержащий ключи каждого индекса в массиве.
     
      const array1 = ['a', 'b', 'c'];
      
const iterator = array1.keys();
      

for (const key of iterator) {
  
           console.log(key);
  // 0,1,2
      }
    // join() объединяет все элементы массива в строку

       const elements = ['1', 2, 3];
       

console.log(elements.join());
//  "1,2,3"
       console.log(elements.join(''));
// "123"
       

console.log(elements.join('-')); // "1-2-3"

    // split - разбивает строку на массив
      // split([separator][, limit]); limit - на которые может разбита строка
        'a,b,c'.split(',',2) // ['a','b']
        'a,b'.split('') // ['a',',','b']
         "some next text".split(' ') // ['some', 'next', 'text']
         "some next text".split('')  // ['some next text']

      //join разбивает маассив на строку
      ['some', 'next','text'].join('') // "somenexttext"
      ['some', 'next','text'].join(' ') // "some next text" 


    // every() проверяет, удовлетворяют ли все элементы массива условию, заданному в передаваемой функции.
       // для пустого массива в любом случае вернет true

      console.log([12, 54, 18, 130, 44].every(item => item >= 10)) // true
      console.log([12, 54, 5, 130, 44].every(item => item >= 10))  // false

   // filter возвращает новый массив удволетворяющий условию
     
       console.log([12, 54, 5, 8, 44].filter(item => item >= 10))) // [12,54,44]

   // flat() возвращает новый массив, в котором все элементы вложенных подмассивов были рекурсивно "подняты" 
    // на указанный уровень depth. depth На сколько уровней вложенности уменьшается мерность исходного массива
   // по умолчанию 1
 
     let arr1 = [1, 2, [3, 4]];
     arr1.flat(); // [1, 2, 3, 4]
        
     let arr2 = [1, 2, [3, 4, [5, 6]]];
     arr2.flat(); // [1, 2, 3, 4, [5, 6]]

     let arr3 = [1, 2, [3, 4, [5, 6]]];
     arr3.flat(2) // [1, 2, 3, 4, 5, 6]
    
     let arr4 = [1, 2, [3, 4, [5, 6, [7, 8, [9, 10]]]]];
     arr4.flat(Infinity); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
     
   // reduce() вычисляет одно значение на основе всего массива, вызывая func для каждого элемента и передавая промежуточный результат между вызовами.
      const arr = [1, 2, 3, 4];
      
console.log(arr.reduce((accumulator, currentValue) => accumulator + currentValue)); // 10

    let initialValue = 0;
    let sum = [{x: 1}, {x:2}, {x:3}].reduce(
              (accumulator, currentValue) => accumulator + currentValue.x, initialValue));  // sum == 6

    // вместо flat развернуть массив
    let flattened = [[0, 1], [2, 3], [4, 5]].reduce((a, b) => a.concat(b)}); // [0, 1, 2, 3, 4, 5]
   
    // склеивание массивов, содержащихся в объектах массива, с использованием оператора расширения и initialValue
    let friends = [ 
		{  books: ["1", "2"] }, 
		{books: ["3", "4"]},
		{ books: ["5", "6"] }
	]

    let allbooks = friends.reduce(function(prev, curr) {
        return [...prev, ...curr.books];
    }, ["0"]); // ['0','1','2','3','4','5','6']

   // reduceRight - все тоже самое только порядок справа- налево все происходит
 
   // flatMap() сначала применяет функцию к каждому элементу, а затем преобразует полученый результат в плоскую структуру 
   // и помещает в новый массив. Это идентично map функции, с последующим применением функции flat с параметром depth 
   // ( глубина ) равным 1
   
      let arr1 = [1, 2, 3, 4];

      arr1.map(x => [x * 2]); // [[2], [4], [6], [8]]
      arr1.flatMap(x => [x * 2]); // [2, 4, 6, 8]
      arr1.flatMap(x => [[x * 2]]); // [[2], [4], [6], [8]]
   
   // fill(value,start,end) заполняет все элементы массива от начального до конечного индексов одним значением.
      [1, 2, 3].fill(4);               // заполняет все значение четверками [4, 4, 4] 
      [1, 2, 3].fill(4, 1);            // заполняет после первого элемента [1, 4, 4]
      [1, 2, 3].fill(4, 1, 2);         // заполняет после первого и до второго [1, 4, 3]
      [].fill.call({ length: 3 }, 4);  // {0: 4, 1: 4, 2: 4, length: 3}
      let  arr = Array(3).fill({}) // [{}, {}, {}];
      arr[0].hi = "hi"; // [{ hi: "hi" }, { hi: "hi" }, { hi: "hi" }]

   // entries() возвращает новый объект итератора массива Array Iterator, 
   // содержащий пары ключ / значение для каждого индекса в массиве.
   let arr = ['a', 'b', 'c'];

   console.log(arr.entries().next().value); // [0, 'a']
   console.log(arr.entries().next().value); // [1, 'b']
   console.log(arr.entries().next().value); // [2, 'c']

  // copyWithin() копирует последовательность элементов массива внутри него в позицию, начинающуюся по индексу target

  [1, 2, 3, 4, 5].copyWithin(0, 3); // копирует числа после 3 индекса в 0 индекс [4, 5, 3, 4, 5] 
  [1, 2, 3, 4, 5].copyWithin(0, 3, 4); // копирует число в 0 индекс число которое аходится между 3 и 4 индексом [4,2,3,4,5]

  // Array.of создаёт новый экземпляр массива Array из произвольного числа
 
     Array.of(7);       // [7] 
     Array.of(1, 2, 3); // [1, 2, 3]

  // Array.from() создаёт новый экземпляр Array из массивоподобного или итерируемого объекта.
    // также может манипулировать со значение как map, не создавая промежуточного массива
    
    console.log(Array.from('foo'));  //  ["f", "o", "o"]
    

console.log(Array.from([1, 2, 3], x => x + x)); // [2, 4, 6]
    console.log(Array.from({ length: 5 }, (v, k) => k)) // [0, 1, 2, 3, 4]
    function f() {
       return Array.from(arguments); // [1,2,3]
    }

    f(1, 2, 3); 
    let m = new Map([[1, 2], [2, 4], [4, 8]]); 
    Array.from(m); // [[1, 2], [2, 4], [4, 8]]
   
   // Array.isArray() возвращает true, если объект является массивом и false, если он массивом не является.
      Array.isArray([]);// true

   // отличие for in от for of в том  как они это делают
     // for in делает  обход перечисляемых свойств объекта осуществляется в произвольном порядке.
     // тоесть выводит имена свойств и методов
        let iterable = [3, 5, 7]; 
        iterable.foo = 'hello';
    
        for (let i in iterable) {
        console.log(i); // выведет 0, 1, 2, "foo"
        }
     // for of обход происходит в соответствии с тем, какой порядок определен в итерируемом объекте.
     // тоесть выводит значения
        let iterable = [3, 5, 7]; 
        iterable.foo = 'hello';
    
        for (let i in iterable) {
        console.log(i); // выведет 3,5,7
        }

     // length длина массива
 
       let arr = [];
       arr[123] = '1';
       console.log(arr.length) // 124

       let arr = [1, 2, 3, 4, 5];
       console.log(arr.length = 2) // [1,2]
       arr.length = 5; // возвращаем length как было
       console.log( arr[3] ); // undefined: значения не восстановились
     

// Set - коллекция уникальных значений
    // плюс set,в том что использование add с одним и тем же значением ничего не происходит
    // также используя set вместо массивов увеличивает производительность, так как у методов set () используется
    // временная сложность O(1) в независимость количество данных сложность алгоритма всегда находится на одном уровне
    // пример (любая арифмитическая операция), а у методов массива поиска, втсавки удаления имеют сложность O(N) 
   // линейная зависимость подразумевает O(n) это значет если количество элементов увеличилось на 1000 то и 
   // количество операции тоже увеличится на 1000 пример (сумма чисел в массиве)
   let obj = {
      a: '1'
   };
   let s = new Set();

   s.add(obj);
   s.has({ a: '1' }); //не найдет так как объект разные


  // как объявить и добавить значения Set

      let text = 'text';
      console.log( new Set(text));  // {'t','e','x'};
      
      let text = 'tex';
      console.log( new Set(text1)); // ['t','e','x']

     с помощью spread можно обратно вернуть в массив
     let set = new Set([1,2,2,6,4,4,5]);
     console.log([...set]) // [1,2,6,4,5]
 
     let set = new Set([1,2,2,6,4,4,5]);
     console.log(set) // {1,2,6,4,5};

     let arr = [1,2,2,6,4,4,5];
     set.add(arr);
     console.log(set) // {1,2,6,4,5}

   // set.delete() удаляет значение, возвращает true иначе false
     
      let set = new Set([1,2,2,6,4,4,5]);
      set.delete(2);
      console.log(set) // {1,6,4,5}
 
   //  set.size – возвращает количество элементов в множестве.

       let set = new Set([1,2,2,6,4,4,5]);
       console.log(set.size); // 5
 
   // set.has() - возвращает true если есть значение иначе false

     let set = new Set([1,2,2,6,4,4,5]);
     console.log(set.has(4)); // true

  // set.clear() – удаляет все имеющиеся значения.
   
     let set = new Set([1,2,2,6,4,4,5]);
     console.log(set.clear());
     console.log(set.size);  // 0

  // Перебор Set 

     let set = new Set([1,2,2,6,4,4,5]);

     for (let value of set) console.log(value); // 1,2,6,4,5

     // то же самое с forEach:
     set.forEach((value, valueAgain, set) => {
         console.log(value);  // 1,2,6,4,5
     });

    // set.values() – возвращает перебираемый объект для значений,
    // set.keys() – то же самое, что и set.values(), присутствует для обратной совместимости с Map,
    // set.entries() – возвращает перебираемый объект для пар вида [значение, значение],
       // присутствует для обратной совместимости с Map.
    
 // Map - коллекция которая хранит пару ключ значение, но в отличии с объектом в качестве ключа может быть все что угодно
    // объявление и добавление Map, так же set и get сохраняет тип ключа 1 и '1' разные ключи
    можем использовать
    const func = () => null;
const object = {};
const array = [];
const bool = false;
const map = new Map();

map.set(func, 'value1');
map.set(object, 'value2');
map.set(array, 'value3');
map.set(bool, 'value4');
map.set(NaN, 'value5');

    //Отличие между объектом и map
    // 1 Обычные объекты ставят нам четкое условие: ключ должен быть строкой (String) или символом (Symbol).  
    //Объекты, массивы, функции, примитивные типы, даже NaN
   // если я делаю obj[123] = true, а затем Object.ключи (obj) тогда я получу ["123"], а не [123]. Карта сохранит тип ключа и вернет [123], что отлично.
   //Объект Map может выполнять итерацию своих элементов в порядке вставки - цикл for..of будет возвращать массив [key, value] для каждой итерации.
   //2 - прямая интерация
   // 
   // А Maps в отличие от объектов прекрасно перебираются напрямую можно использовать ниже методы set,sizeb тд, или же for of
   //для перебора значений
   //что у обычного олбъекта узнать размер колекции нужно использовать Object.keys({}).length, а для map просто map.size()
   //Элементы Map итерируемы в порядке вставки. а объект нет и нужно использовать
//map более быстрый чем объект для объектов Сначала нужно преобразовать его в некоторое подобие массива, используя Object.keys(), Object.values() или Object.entries(). или же for in использовать но он перебирает только свойства и не работает с полями-символами;

//Есть и другие различия. Например, объекты легче сериализовать, хранить, передавать и восстанавливать (при помощи JSON) и они лучше подходят для создания элементов с определённой логикой и поведением (у объектов есть методы и внутренняя связь между методами и данными через this). Для Map легче получать размер структуры, они поддерживают итерацию без дополнительных усилий.

         let map = new Map([[1,1],[2,2],[3,3]]);
       console.log([...map]) // [ 0:[1,1], 1:[2,2], 2:[3,3] ]
 
       let map = new Map([[1,1],[2,2],[3,3]]);
       console.log(map); Map(3) {1 => 1, 2 => 2, 3 => 3}

       let map = new Map()
       
       map.set(1,1).set(2,2).set(3,3) // цепочку можно построить  Map(3) {1 => 1, 2 => 2, 3 => 3}
    
       let map = new Map();

       map.set(1,2);
       map.set(2,2);
       map.set(3,3);
       console.log(map) //Map(3) {1 => 1, 2 => 2, 3 => 3}

     // map.size - позволяет получить количество элементов
    
        let map = new Map()
       
        map.set(1,1).set(2,2).set(3,3);
        console.log(map.size) // 3

     // map.get - позврляет получить значения по ключу

         let map = new Map()
       
         map.set(2,1).set('2',2).set(3,3);
         console.log(map.get('2')) //2

     // map.clear - удаляет все записи коллекции
       
        
        let map = new Map()
       
         map.set(2,1).set('2',2).set(3,3);
         map.clear()
         console.log(map.size) //0

     // map.delete(key) удаляет запись с ключом key, возвращает true, если такая запись была, иначе false.
       
        let map = new Map()
       
         map.set(2,1).set('2',2).set(3,3);
         
         console.log(map.delete(2)) //true
    
     // map.has(key) – возвращает true, если ключ есть, иначе false.
    
        let map = new Map()
       
         map.set(2,1).set('2',2).set(3,3);
         
         console.log(map.has(4)) //false

   // Интерация

       map.keys() – возвращает итерируемый объект для ключей,
       map.values() – возвращает итерируемый объект для значений,
       map.entries() – возвращает итерируемый объект для записей [ключ, значение], он используется по умолчанию в for..of.
      
       let map = new Map()
       map.set(1,1).set(2,2).set(3,3);
      // цикл по ключам
        for(let key of map.keys()) {
           console.log(key); // 1,2,3
         }

      // цикл по значениям
      for(let value of map.values()) {
         console.log(value); // 1,2,3
      }

       // цикл по записям [ключ,значение]
       for(let entry of map) { // то же что и recipeMap.entries()
          console.log(entry); // [1,1], [2,2],[3,3]
       }
       
     // через forEach
        map.forEach( (value, key, map) => {
                 console.log(`${key}: ${value}`); // 1:1, 2:2, 3:3
         });
       
    // Объект из Map 
      let map = new Map()
;

       map.set(1,1).set(2,2).set(3,3);

      console.log(Object.fromEntries(map)) // {1: 1, 2: 2, 3: 3}
       
   // map из объекта
      let obj = {
        1: 1,
        2: 2,
        3:3
      };

    console.log( new Map(Object.entries(obj))) //Map(3) {"1" => 1, "2" => 2, "3" => 3}

   // spread и rest операторы

      // rest оператор остатка используется для сбора элементов в коллецию
        let sum = (...args) => args.reduce((s, num) => s + num, 0);
        sum(1,2,3,4,5); // 15
        
        const myString = 'hi';
        const array = [...myString] // ['h','i'],
     
        //исключает свойство password
        const noPassword = ({ password, ...rest }) => rest
        const user = {
             id: 1,
             age: '21',
             password: 'Password!'
        }
        noPassword(user) //=> { id: 1, age: '21' }

      // spread - оператор используется для разделения коллекции на элементы
         let arr = [1, 2, 3, 4, 5];
         let data = [...arr];
         let copy = arr;

         arr === data; // false − ссылки отличаются − два разных массива
         arr === copy; // true − две переменные ссылаются на один массив
       
        //Преобразование DOM коллекции в массив
         
         let links = [...document.querySelectorAll('a')]; // [a,a,a]
         Array.isArray(links); // true

         let arr = [2, 3];
         let  data = [1, ...arr,4, 5];
         console.log(data); // [1,2,3,4,5]

         let obj1 = {a: 1, b: 2};
         let obj2 = {...obj1, c: 3, d: 4};

         console.log(obj2); { a: 1, b: 2, c: 3, d: 4 }

         let clone = {...obj1}; // склонирует объект (поверхностно) Новый объект создастся, 
        // но клонированные свойства все равно будут от оригинала, а не клонами, альтернатива Object.assign({},obj1)
  
         let obj1 = {a: 1, name:'Artem'};
         let obj2 = {a: 2, age:21};
         let obj3 = {...obj1, ...obj2}; // {a: 2,name:'Artem' age:21} a:2 так как перезаписался

         let obj4 = [1,2,3];
         let obj5 = [1,2,3];
         console.log([...obj4, ...obj5]) // [1,2,3,1,2,3]
 
          console.log([...obj4, ...obj1]) //{0: "1", 1: "2", 2: "3", a: 1, name: "Artem"}
         
 
// Деструктуризация - позволяет разбивать объект или массив на переменные при присвоении.
// //деструктаризация
// let obj ={id:1,name:"dima", passport:{id:1}}
// let {passport,...user} = obj;

// passport // {id:1}
// user // {id:1,name:"dima"}

// let  obj = {a:10,data:[1,2,3,4]};

// let {0:id,1:age, ...re} = obj.data; // 0,1 элементы от каккого начинаются
// console.log(id, age, re); //1 2 {2:3,3:4}

    let mas = [1,2,3,4]
    let [a, b, ...othersValue] = mas;
   
let obj = {a:1, b:'3', '':true}
let {a, b, ['']:m } = obj
console.log(a,b,m)

    let obj = {id:1, name:'Artem', secondName:'Rak', age:21}
    let {name, ...othersValue} = obj;

    // пропуск элементов
    let mas = [1,2,3,4]
    let [a, ,...othersValue] = mas // 1, [3,4]

   // присвайвание
     let user = {};
     let mas = ['Artemiy','Rak']
    [user.name, user.surname] = mas; // {name:'Artem',surname:'Rak'}
    
   // значения по умолчанию
     let [name = "Guest", surname = "Anonymous"] = ["Artem"];
     console.log(name, surname) // Artem Anonymous

     let options = {
        title: "Menu"
     };
  
    let {width = 100, title} = options;
    console.log(width, title) // 100,  'Menu'


// Object – это ссылочный тип данных , представляющий собой набор свойств(ключ:значение, ключ:обьект) и методов
   const obj1 = {
          name: 'Artemiy'
   } // литеральная нотация

   const obj2 = {
         name: {
         firstName: 'Artemiy',
         secondName: 'Rak'
    },
       getName: function() {
            console.log("Меня зовут: " + this.name.firstName);
       }
    }
через new как создаеться
и ка js работает
создаст новый объект и назначет в качестве this,
потом для этого объекта служебное свойство prototype выставит тот объект который храниться в свойстве функции
function Lang (name) {
//this = {};
// this.[[Prototype]] = Lang.prototype
this.name = name;
//return this
}

new Lang('js') // {name:js} 
    obj2.getName(); //Меня зовут:  Artemiy

    const obj3 = new Object(); // с помощью конструктора new
    obj3.name = "Valera";
    const obj4 = new Object();
    obj4.name1 = new Object();
    obj4.name1.firstName = 'Artemiy';
    obj4.getName = function() {
       console.log("Меня зовут: " + this.name1.firstName);
    }

    obj4.getName(); // Меня зовут:  Artemiy


    //другой способ создания через конструктор new
    function Name(firstName, secondName) {
         this._firstName = firstName;
         this._secondName = secondName;

         this.getName = function() {
             console.log("Меня зовут: " + this._firstName);
         };
     }

     let name = new Name("Artemiy");
     name.getName(); // Меня зовут: Artemiy
    
    // присваивание значения свойству
       let user = {id:1};
    
       user["likes birds"] = true;
   
    // удалить свойство можно так
       delete user["likes birds"];
      delete user.id;

   // зарезервированные слова для свойств не помеха
     
   // in используется для проверки свойства в объекте

     let user = {  age: 30 };
     console.log( "age" in user );

   
     for (let key in user) {
       console.log( key );  // age
       console.log( user[key] ); //  30
     }
  
  // Принцип достижимости 
       let user = {
          name: "Artem"
       };

       let admin = user;

       user = null;
       console.log(admin.name)  // Artem доступен через глобальную переменную admin, поэтому он находится в памяти
  
  // Оператор опциональной последовательности ?. 
  // позволяет получить значение свойства, находящегося на любом уровне вложенности в цепочке связанных между
  //  собой объектов, без необходимости проверять каждое из промежуточных свойств в ней на существование
  // проверяет на undefined и null
      let user1 = {
        firstName: "Иван",
        getName() {console.log(this.firstName)}
      };

      console.log( user1?.firstName ); // Иван
      console.log( user1?.secondName );
      console.log( user1.getName?.() );

   // Методы 
    let o =  Object.assign({},{name:'Artem'});    // позволяет клоннировать объект
 
    Object.create() //создаёт новый объект с указанным прототипом и свойствами.
    Object.create(null) //создает без протатипа 
    let  o = Object.create({}, { p: { value: 42 },p: { value: 'Artem', writable:true } }); 
     // после value можно указать доп свойства
    //  writable: true, свойство можно изменить, иначе оно только для чтения
     // enumerable: true, свойство перечисляется в циклах, в противном случае циклы его игнорируют.
     // configurable: true свойство можно удалить, а эти атрибуты можно изменять, иначе этого делать нельзя.
  
      var user = {
	sayHi: function() {
		alert('Привет. Меня зовут ' + this.name);
	}
       }

      let artem = Object.create(user);
      artem.name = 'Артем';

      artem.sayHi() //'Привет. Меня зовут  Артем'
     
      // объект будет выглядеть так
        let artem = {
	name: 'Artem',
	__proto__: user,
       }

    // Object.freeze() замораживает объект: это значит, что он предотвращает добавление новых свойств к объекту,
    //  удаление старых свойств из объекта и изменение существующих свойств
    // замораживает только на первых уровнях
    let obj = {
       internal: {name:Artem} //не замораживает
    };

    // Object.isFrozen() определяет, был ли объект заморожен.
    // Object.fromEntries() преобразует список пар ключ-значение в объект.
 
    const entries = new Map([
  ['foo', 'bar'],
  ['baz', 42]
]);
    

const obj = Object.fromEntries(entries);
    

console.log(obj);
// { foo: "bar", baz: 42 }

   //  Object.is() определяет, являются ли два значения одинаковыми значениями.
     Object.is('foo', 'foo');  // true

   // Object.keys()  возвращает массив строковых элементов если это массив -> массив индекс, в объекте ключи
      let arr = ['a', 'b', 'c'];
      console.log(Object.keys(arr)); // ['0', '1', '2'] 

   // Object.values() возвращает массив значений перечисляемых свойств объекта
    let  obj = { foo: "bar", baz: 42 };
    console.log(Object.values(obj)); // ['bar', 42]
   
//Метод Object.defineProperty() определяет новое или изменяет существующее 
//свойство непосредственно на объекте, возвращая этот объект. 

Object.defineProperty(obj, prop, descriptor)
//obj Объект, на котором определяется свойство.
//prop Имя определяемого или изменяемого свойства.
//descriptor Дескриптор определяемого или изменяемого свойства.

 //  writable: true, свойство можно изменить, иначе оно только для чтения
     // enumerable: true, свойство перечисляется в циклах, в противном случае циклы его игнорируют.
     // configurable: true свойство можно удалить, а эти атрибуты можно изменять, иначе этого делать нельзя.
var o = {};
Object.defineProperty(o, 'a', {
  value: 37,
  writable: true,
  enumerable: true,
  configurable: true
});

// Пример добавления свойства к объекту через defineProperty()
// с дескриптором доступа
var bValue = 38;
Object.defineProperty(o, 'b', {
  get: function() { return bValue; },
  set: function(newValue) { bValue = newValue; },
  enumerable: true,
  configurable: true
});
o.b; // 38


function Archiver() {
  var temperature = null;
  var archive = [];

  Object.defineProperty(this, 'temperature', {
    get: function() {
      console.log('get!');
      return temperature;
    },
    set: function(value) {
      temperature = value;
      archive.push({ val: temperature });
    }
  });

  this.getArchive = function() { return archive; };
}

var arc = new Archiver();
arc.temperature; // 'get!'
arc.temperature = 11;
arc.temperature = 13;
arc.getArchive(); // [{ val: 11 }, { val: 13 }]

//Метод Object.defineProperties() определяет новые или изменяет существующие свойства, 
//непосредственно на объекте, возвращая этот объект.

Object.defineProperties(obj, {
  'property1': {
    value: true,
    writable: true
  },
  'property2': {
    value: 'Hello',
    writable: false
  }
  // и т.д.
});

let object = {};
Object.defineProperty(object, 'method',{value:function() {}})
Object.defineProperty(object, 'value',{value:42, confiturable:true})
по умолчанию если не указать writable, confirtuble, enurable, то установит везде false

установка аксессоров
Object.defineProperty(object, 'property',{get:function() {return this.value},set:function(value) {return this.value=value},enumarable:true, writable:true});

object.property = 42 // set 42
object.property // get 42

//новый формат
let obj = {
get:function() {return this.value},
set:function(value) {return this.value=value}
}

[[Prototype]] - служебное свойство для переиспользования кода, когда свойства нет в объекте, то происходит поиск по цепочке прототипов

Установка Prototype
в служебное свойство __proto__ мы можем установить

let prototype = {value:42}:
let obj = {};

object.__proto__ = prototype;
object.value;

Object.setPrototypeOf(object,prototype) //явное установка
Object.getPrototypeOf(object) // получение

// установка значения в прототип
она создает новый объект и проставляет то значение которое передадим первый аргументом, есть другие это дополнительные дескрипторы
let prototype = {value:42}:
const object = Object.create(prototype);

object.value //42
 
